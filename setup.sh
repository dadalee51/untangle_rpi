#!/bin/sh

echo "before running this script, please make sure your keyboard and wifi"
echo " has been setup correctly, i2c and ssh enabled via raspi-config"
echo " and enable uart in boot config, remove the serial console from cmdline.txt"
echo "first, upgrade the apt"
apt-get update
apt-get install -y git build-essential python-dev python-smbus python-pip ppp  libboost1.50-all 
git clone https://github.com/adafruit/Adafruit_Python_BMP.git
cd Adafruit_Python_BMP
sudo python setup.py install
cd ..

pip install pyserial numpy 
pip install futures requests

git clone -b pi3 https://github.com/dadalee51/untangle_rpi

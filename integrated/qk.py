#!/usr/bin/python

import numpy as np


class QuickKalman:
	def __init__(self, q, r, p):
		self.q=q 	#process noise covariance
		self.r=r 	#measurement noise covariance
		self.p=p 	#estimation error covariance
		self.x=0.0	#resulted value
		self.k=0.0 	#gain
	
	#get next kalman prediction
	def next(self, input):
		#prediction update
		self.p = self.p + self.q
		#measurement update
		self.k = self.p / (self.p + self.r)
		self.x = self.x + self.k * (input - self.x)
		self.p = (1 - self.k) * self.p
		return self.x

	#testing function
	def test(self):	
		print ("start testing")
		qk = QuickKalman()
		for x in range(0, 10):
			radicalValue = np.random.normal(x,10)
			print(str(radicalValue) + "," +  str(qk.next(radicalValue)))



package main_test

import "fmt"

func ExampleBitSet() {
	//fmt.Println("we want to read abyte and change its bit to match maskbit...")
	var oldv uint8 = 0x33              //0b00110011
	var mask uint8 = 0x1C              //0b00011100
	var targ uint8 = 0x0C              //0b00001100
	var newv uint8 = oldv&^mask | targ //expect 101111
	fmt.Printf("%b", newv)
	//Output: 101111
}

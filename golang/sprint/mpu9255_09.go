package main

import (
	"fmt"
	"time"

	"github.com/kidoman/embd"
	_ "github.com/kidoman/embd/host/all"
)

//utility method
//delay method
func delay(ms int) {
	time.Sleep(time.Duration(ms) * time.Millisecond)
}

//example: setBitsToReg(MP9150_DEFAULT_ADDRESS, MPU9150_RA_USER_CTRL, (1<<MPU9150_USERCTRL_FIFO_EN_BIT) | (1<<MPU9150_USERCTRL_I2C_MST_EN_BIT), 0xFF)
//input is masked, 0xFF will adhere to the two bit position, in this case set the two position to TRUE.
//will read from the register, bit clear and OR the masked bits, write back to register.
func setBitsToReg(device uint8, register uint8, mask uint8, value uint8, bus embd.I2CBus) {
	//expect bit masking, not bit position
	old, _ := bus.ReadByteFromReg(device, register)
	hld := old&^mask | value
	bus.WriteByteToReg(device, register, hld)
}

//readBitsFromReg - output a uint8 of result
func readBitsFromReg(device uint8, register uint8, mask uint8, bus embd.I2CBus) uint8 {
	//expect bit masking, not bit position
	old, _ := bus.ReadByteFromReg(device, register)
	hld := old &^ mask
	return hld
}

//port from the known working Arduino code base.
func main() {
	dmpReady := false        // set true if DMP init was successful
	var mpuIntStatus uint8   // holds actual interrupt status byte from MPU
	var devStatus uint16     // return status after each device operation (0 = success, !0 = error)
	var packetSize uint16    // expected DMP packet size (default is 42 bytes)
	var fifoCount uint16     // count of all bytes currently in FIFO
	var fifoBuffer [64]uint8 // FIFO storage buffer

	// orientation/motion vars
	var q Quaternion        // [w, x, y, z]         quaternion container
	var aa VectorInt16      // [x, y, z]            accel sensor measurements
	var aaReal VectorInt16  // [x, y, z]            gravity-free accel sensor measurements
	var aaWorld VectorInt16 // [x, y, z]            world-frame accel sensor measurements
	var gravity VectorFloat // [x, y, z]            gravity vector
	var euler [3]float32    // [psi, theta, phi]    Euler angle container
	var ypr [3]float32      // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector

	fmt.Println("Initialize the mpu9255 modules...")
	//var data [6]byte
	//var wg sync.WaitGroup

	bus := embd.NewI2CBus(1)
	defer embd.CloseI2C()
	//init i2c device
	//clock source
	setBitsToReg(devAddr, MPU9150_RA_PWR_MGMT_1, MPU9150_PWR1_CLKSEL_BIT, MPU9150_CLOCK_PLL_XGYRO, bus)
	//gyro range
	setBitsToReg(devAddr, MPU9150_RA_GYRO_CONFIG, MPU9150_GCONFIG_FS_SEL_BIT, MPU9150_GYRO_FS_250, bus)
	//accel range
	setBitsToReg(devAddr, MPU9150_RA_ACCEL_CONFIG, MPU9150_ACONFIG_AFS_SEL_BIT, MPU9150_ACCEL_FS_2, bus)
	//disable sleep
	setBitsToReg(devAddr, MPU9150_RA_PWR_MGMT_1, MPU9150_PWR1_SLEEP_BIT, 0x00, bus)

	//init DMP device
	devStatus = dmpInitialize(bus)

	if devStatus == 0 {
		// turn on the DMP, now that it's ready
		fmt.Println("Enabling DMP...")
		setDMPEnabled(true, bus)

		mpuIntStatus = getIntStatus(bus)

		// set our DMP Ready flag so the main loop() function knows it's okay to use it
		fmt.Println("DMP ready! Waiting for first interrupt...")
		dmpReady = true

		// get expected DMP packet size for later comparison
		packetSize = 48
	} else {
		// ERROR!
		// 1 = initial memory load failed
		// 2 = DMP configuration updates failed
		// (if it's going to break, usually the code will be 1)
		fmt.Print("DMP Initialization failed (code ")
		fmt.Print(devStatus)
		fmt.Println(")")
	}

	setRate(50, bus)

	for {
		// if programming failed, don't try to do anything
		if !dmpReady {
			return
		}

		// reset interrupt flag and get INT_STATUS byte

		mpuIntStatus = getIntStatus(bus)

		// get current FIFO count
		fifoCount = getFIFOCount(bus)

		// check for overflow (this should never happen unless our code is too inefficient)
		if (mpuIntStatus&0x10) == 0x10 || fifoCount == 1024 {
			// reset so we can continue cleanly
			resetFIFO(bus)
			fmt.Println("FIFO overflow!")

			// otherwise, check for DMP data ready interrupt (this should happen frequently)
		} else if (mpuIntStatus & 0x02) == 0x02 {
			// wait for correct available data length, should be a VERY short wait
			for fifoCount < packetSize {
				fifoCount = getFIFOCount(bus)
			}

			// read a packet from FIFO
			getFIFOBytes(fifoBuffer, packetSize, bus)

			// track FIFO count here in case there is > 1 packet available
			// (this lets us immediately read more without waiting for an interrupt)
			fifoCount -= packetSize

			// display quaternion values in easy matrix form: w x y z
			dmpGetQuaternionQua(q, fifoBuffer)
			fmt.Print("quat\t")
			fmt.Print(q.w)
			fmt.Print("\t")
			fmt.Print(q.x)
			fmt.Print("\t")
			fmt.Print(q.y)
			fmt.Print("\t")
			fmt.Println(q.z)

			delay(100)
		}

	}
}

func dmpInitialize(bus embd.I2CBus) uint16 {
	// reset device
	reset(bus)
	delay(30) // wait after reset
	//disable sleep
	setBitsToReg(devAddr, MPU9150_RA_PWR_MGMT_1, MPU9150_PWR1_SLEEP_BIT, 0x00, bus)
	setMemoryBank(0x10, true, true, bus)
	setMemoryStartAddress(0x06, bus)
	hwRevision := readMemoryByte(bus)
	setMemoryBank(0, false, false, bus)
	xgOffset := getXGyroOffset(bus)
	ygOffset := getYGyroOffset(bus)
	zgOffset := getZGyroOffset(bus)
	var buffer []uint8
	bus.ReadFromReg(devAddr, MPU9150_RA_USER_CTRL, buffer) // ?
	bus.WriteByteToReg(devAddr, MPU9150_RA_INT_PIN_CFG, 0x32)
	bus.WriteByteToReg(0x0E, 0x0A, 0x00)
	bus.WriteByteToReg(0x0E, 0x0A, 0x0F)
	var asax, asay, asaz int8
	bus.ReadFromReg(0x0E, 0x10, buffer)
	asax = int8(buffer[0])
	asay = int8(buffer[1])
	asaz = int8(buffer[2])
	bus.WriteByteToReg(0x0E, 0x0A, 0x00)
	if writeProgMemoryBlock(dmpMemory, MPU9150_DMP_CODE_SIZE, 0, 0, true, bus) {
		if writeProgDMPConfigurationSet(dmpConfig, MPU9150_DMP_CONFIG_SIZE, bus) {
			setIntEnabled(0x12, bus)
			setRate(4, bus) // 1khz / (1 + 4) = 200 Hz
			//setClockSource(MPU9150_CLOCK_PLL_ZGYRO, bus)
			setBitsToReg(devAddr, MPU9150_RA_PWR_MGMT_1, MPU9150_PWR1_CLKSEL_BIT, MPU9150_CLOCK_PLL_ZGYRO, bus)
			setDLPFMode(MPU9150_DLPF_BW_42, bus)
			setExternalFrameSync(MPU9150_EXT_SYNC_TEMP_OUT_L, bus)
			//setFullScaleGyroRange(MPU9150_GYRO_FS_2000, bus)
			setBitsToReg(devAddr, MPU9150_RA_GYRO_CONFIG, MPU9150_GCONFIG_FS_SEL_BIT, MPU9150_GYRO_FS_2000, bus)
			setDMPConfig1(0x03, bus)
			setDMPConfig2(0x00, bus)
			setOTPBankValid(false, bus)
			setXGyroOffsetTC(xgOffset, bus)
			setYGyroOffsetTC(ygOffset, bus)
			setZGyroOffsetTC(zgOffset, bus)
			setXGyroOffset(0, bus)
			setYGyroOffset(0, bus)
			setZGyroOffset(0, bus)
			bus.WriteByteToReg(0x68, MPU9150_RA_PWR_MGMT_2, 0x00)
			bus.WriteByteToReg(0x68, MPU9150_RA_ACCEL_CONFIG, 0x00)
			setMotionDetectionThreshold(2, bus)
			setZeroMotionDetectionThreshold(156, bus)
			setMotionDetectionDuration(80, bus)
			setZeroMotionDetectionDuration(0, bus)
			bus.WriteByteToReg(0x0E, 0x0A, 0x01)
			bus.WriteByteToReg(0x68, MPU9150_RA_I2C_SLV0_ADDR, 0x8E)
			bus.WriteByteToReg(0x68, MPU9150_RA_I2C_SLV0_REG, 0x01)
			bus.WriteByteToReg(0x68, MPU9150_RA_I2C_SLV0_CTRL, 0xDA)
			bus.WriteByteToReg(0x68, MPU9150_RA_I2C_SLV2_ADDR, 0x0E)
			bus.WriteByteToReg(0x68, MPU9150_RA_I2C_SLV2_REG, 0x0A)
			bus.WriteByteToReg(0x68, MPU9150_RA_I2C_SLV2_CTRL, 0x81)
			bus.WriteByteToReg(0x68, MPU9150_RA_I2C_SLV2_DO, 0x01)
			bus.WriteByteToReg(0x68, MPU9150_RA_I2C_SLV4_CTRL, 0x18)
			bus.WriteByteToReg(0x68, MPU9150_RA_I2C_MST_DELAY_CTRL, 0x05)
			bus.WriteByteToReg(0x68, MPU9150_RA_INT_PIN_CFG, 0x00)
			bus.WriteByteToReg(0x68, MPU9150_RA_USER_CTRL, 0x20)
			bus.WriteByteToReg(0x68, MPU9150_RA_USER_CTRL, 0x24)
			bus.WriteByteToReg(0x68, MPU9150_RA_USER_CTRL, 0x20)
			bus.WriteByteToReg(0x68, MPU9150_RA_USER_CTRL, 0xE8)

			setDMPEnabled(false, bus)
			dmpPacketSize := 48
			resetFIFO(bus)
			getIntStatus(bus)
		} else {
			return 2 // configuration block loading failed
		}
	} else {
		return 1 // main binary block loading failed
	}
	return 0 // success
}

// PWR_MGMT_1 register
func reset(bus embd.I2CBus) {
	setBitsToReg(devAddr, MPU9150_RA_PWR_MGMT_1, MPU9150_PWR1_DEVICE_RESET_BIT, 0x80, bus)
}

// BANK_SEL register
func setMemoryBank(bank uint8, prefetchEnabled bool, userBank bool, bus embd.I2CBus) {
	bank &= 0x1F
	if userBank == true {
		bank |= 0x20
	}
	if prefetchEnabled == true {
		bank |= 0x40
	}
	bus.WriteByteToReg(devAddr, MPU9150_RA_BANK_SEL, bank)
}

// MEM_START_ADDR register
func setMemoryStartAddress(address uint8, bus embd.I2CBus) {
	bus.WriteByteToReg(devAddr, MPU9150_RA_MEM_START_ADDR, address)
}

// MEM_R_W register
func readMemoryByte(bus embd.I2CBus) uint8 {
	buffer, _ := bus.ReadByteFromReg(devAddr, MPU9150_RA_MEM_R_W)
	return buffer
}

func getXGyroOffsetTC(bus embd.I2CBus) uint8 {
	buffer := readBitsFromReg(devAddr, MPU9150_RA_XG_OFFS_TC, MPU9150_TC_OFFSET_BIT, bus)
	return buffer
}

// YG_OFFS_TC register
func getYGyroOffsetTC(bus embd.I2CBus) uint8 {
	buffer := readBitsFromReg(devAddr, MPU9150_RA_YG_OFFS_TC, MPU9150_TC_OFFSET_BIT, bus)
	return buffer
}

// ZG_OFFS_TC register
func getZGyroOffsetTC(bus embd.I2CBus) uint8 {
	buffer := readBitsFromReg(devAddr, MPU9150_RA_ZG_OFFS_TC, MPU9150_TC_OFFSET_BIT, bus)
	return buffer
}

//get offset non TC
func getXGyroOffset(bus embd.I2CBus) uint16 {
	var buffer []uint8
	bus.ReadFromReg(devAddr, MPU9150_RA_XG_OFFS_USRH, buffer)
	return (uint16(buffer[0]) << 8) | uint16(buffer[1])
}

// YG_OFFS_USR* register
func getYGyroOffset(bus embd.I2CBus) uint16 {
	var buffer []uint8
	bus.ReadFromReg(devAddr, MPU9150_RA_YG_OFFS_USRH, buffer)
	return (uint16(buffer[0]) << 8) | uint16(buffer[1])
}
func getZGyroOffset(bus embd.I2CBus) uint16 {
	var buffer []uint8
	bus.ReadFromReg(devAddr, MPU9150_RA_ZG_OFFS_USRH, buffer)
	return (uint16(buffer[0]) << 8) | uint16(buffer[1])
}

//set offsets
func setXGyroOffset(offset uint16, bus embd.I2CBus) {
	bus.WriteWordToReg(devAddr, MPU9150_RA_XG_OFFS_USRH, offset)
}

// YG_OFFS_USR* register
func setYGyroOffset(offset uint16, bus embd.I2CBus) {
	bus.WriteWordToReg(devAddr, MPU9150_RA_YG_OFFS_USRH, offset)
}

// ZG_OFFS_USR* register
func setZGyroOffset(offset uint16, bus embd.I2CBus) {
	bus.WriteWordToReg(devAddr, MPU9150_RA_ZG_OFFS_USRH, offset)
}

func readMemoryBlock(data []uint8, dataSize int, bank uint8, address uint8, bus embd.I2CBus) {
	//not needed...
}
func writeMemoryBlock(data []uint8, dataSize int, bank uint8, address uint8, bus embd.I2CBus) bool {
	var bankPT uint8 = bank
	setMemoryBank(bankPT, true, true, bus)
	setMemoryStartAddress(address, bus)
	chunkSize := MPU9150_DMP_MEMORY_CHUNK_SIZE
	for i := 0; i < dataSize; i++ {
		bus.WriteToReg(devAddr, MPU9150_RA_MEM_R_W, data[i])
		setMemoryBank(bankPT, true, true, bus)
		setMemoryStartAddress(address+i, bus)
		if i%256 == 0 {
			bankPT++
		}
	}
	return true
}

func setIntEnabled(enabled uint8, bus embd.I2CBus) {
	bus.WriteByteToReg(devAddr, MPU9150_RA_INT_ENABLE, enabled, bus)
}

func testConnection(bus embd.I2CBus) bool {
	if getDeviceID(bus) == 0x34 {
		return true
	}
}

// SMPLRT_DIV register
func setRate(rate uint8, bus embd.I2CBus) {
	bus.WriteByteToReg(devAddr, MPU9150_RA_SMPLRT_DIV, rate)
}

func setDLPFMode(mode uint8, bus embd.I2CBus) {
	setBitsToReg(devAddr, MPU9150_RA_CONFIG, MPU9150_CFG_DLPF_CFG_BIT, mode, bus)
}

func setExternalFrameSync(sync uint8, bus embd.I2CBus) {
	setBitsToReg(devAddr, MPU9150_RA_CONFIG, MPU9150_CFG_EXT_SYNC_SET_BIT, sync, bus)
}
func setDMPConfig1(config uint8, bus embd.I2CBus) {
	bus.WriteByteToReg(devAddr, MPU9150_RA_DMP_CFG_1, config)
}

func setDMPConfig2(config uint8, bus embd.I2CBus) {
	bus.WriteByteToReg(devAddr, MPU9150_RA_DMP_CFG_2, config)
}

func setOTPBankValid(enabled bool, bus embd.I2CBus) {
	var data uint8
	if enabled == true {
		data = 0xFF
	} else {
		data = 0x00
	}
	setBitsToReg(devAddr, MPU9150_RA_XG_OFFS_TC, MPU9150_TC_OTP_BNK_VLD_BIT, data, bus)
}
func setXGyroOffsetTC(offset uint16, bus embd.I2CBus) {
	setBitsToReg(devAddr, MPU9150_RA_XG_OFFS_TC, MPU9150_TC_OFFSET_BIT, offset, bus)
}
func setYGyroOffsetTC(offset uint16, bus embd.I2CBus) {
	setBitsToReg(devAddr, MPU9150_RA_YG_OFFS_TC, MPU9150_TC_OFFSET_BIT, offset, bus)
}
func setZGyroOffsetTC(offset uint16, bus embd.I2CBus) {
	setBitsToReg(devAddr, MPU9150_RA_ZG_OFFS_TC, MPU9150_TC_OFFSET_BIT, offset, bus)
}
func setXGyroOffset(offset uint16, bus embd.I2CBus) {
	bus.WriteWordToReg(devAddr, MPU9150_RA_XG_OFFS_USRH, offset)
}
func setYGyroOffset(offset uint16, bus embd.I2CBus) {
	bus.WriteWordToReg(devAddr, MPU9150_RA_YG_OFFS_USRH, offset)
}
func setZGyroOffset(offset uint16, bus embd.I2CBus) {
	bus.WriteWordToReg(devAddr, MPU9150_RA_ZG_OFFS_USRH, offset)
}
func resetFIFO(bus embd.I2CBus) {
	setBitsToReg(devAddr, MPU9150_RA_USER_CTRL, MPU9150_USERCTRL_FIFO_RESET_BIT, true, bus)
}
func getFIFOCount(bus embd.I2CBus) uint16 {
	bus.ReadFromReg(devAddr, MPU9150_RA_FIFO_COUNTH, buffer)
	return ((uint16_t(buffer[0])) << 8) | buffer[1]
}
func setMotionDetectionThreshold(threshold uint8, bus embd.I2CBus) {
	bus.WriteByteToReg(devAddr, MPU9150_RA_MOT_THR, threshold)
}
func setZeroMotionDetectionThreshold(threshold uint8, bus embd.I2CBus) {
	bus.WriteByteToReg(devAddr, MPU9150_RA_ZRMOT_THR, threshold)
}
func setMotionDetectionDuration(duration uint8, bus embd.I2CBus) {
	bus.WriteByteToReg(devAddr, MPU9150_RA_MOT_DUR, duration)
}
func setZeroMotionDetectionDuration(duration uint8, bus embd.I2CBus) {
	bus.WriteByteToReg(devAddr, MPU9150_RA_ZRMOT_DUR, duration)
}
func getFIFOBytes(data [64]uint8, length uint16, bus embd.I2CBus) {
	bus.ReadByteFromRegs(devAddr, MPU9150_RA_FIFO_R_W, data)
}

// INT_STATUS register
func getIntStatus(bus embd.I2CBus) uint8 {
	bus.ReadByteFromReg(devAddr, MPU9150_RA_INT_STATUS, buffer)
	return buffer
}
func setDMPEnabled(enabled bool, bus embd.I2CBus) {
	var data uint8
	if enabled == true {
		data = 0xFF
	} else {
		data = 0x00
	}
	setBitsToReg(devAddr, MPU9150_RA_USER_CTRL, MPU9150_USERCTRL_DMP_EN_BIT, data, bus)
}

func dmpGetQuaternion32(data []int32, packet [64]uint8) uint8 {
	if packet == 0 {
		packet = dmpPacketBuffer
	}
	data[0] = ((packet[0] << 24) + (packet[1] << 16) + (packet[2] << 8) + packet[3])
	data[1] = ((packet[4] << 24) + (packet[5] << 16) + (packet[6] << 8) + packet[7])
	data[2] = ((packet[8] << 24) + (packet[9] << 16) + (packet[10] << 8) + packet[11])
	data[3] = ((packet[12] << 24) + (packet[13] << 16) + (packet[14] << 8) + packet[15])
	return 0
}
func dmpGetQuaternion16(data []int16, packet [64]uint8) uint8 {
	if packet == 0 {
		packet = dmpPacketBuffer
	}
	data[0] = ((packet[0] << 8) + packet[1])
	data[1] = ((packet[4] << 8) + packet[5])
	data[2] = ((packet[8] << 8) + packet[9])
	data[3] = ((packet[12] << 8) + packet[13])
	return 0
}
func dmpGetQuaternionQua(q Quaternion, packet [64]uint8) uint8 {
	var qI [4]int16
	status := dmpGetQuaternion16(qI, packet)
	if status == 0 {
		q.w = float(qI[0]) / 16384.0
		q.x = float(qI[1]) / 16384.0
		q.y = float(qI[2]) / 16384.0
		q.z = float(qI[3]) / 16384.0
		return 0
	}
	return status // int16 return value, indicates error if this line is reached
}

//-------------END OF EXTRACTION ----------------------------

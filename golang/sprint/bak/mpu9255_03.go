package main

import "github.com/kidoman/embd"
import _ "github.com/kidoman/embd/host/all"
import "fmt"
//import "strconv"
import "time"

func main(){
	fmt.Println("Initialize the mpu9255 modules...")

	const mpu9255 = 0x68
	const magcomp = 0x0C

	bus := embd.NewI2CBus(1)
	defer embd.CloseI2C()
	
	//we want to send a power reset to 0x6B
	bus.WriteByteToReg(mpu9255, 0x6B, 0x00)

	//then set the bypass enabled for magnetic compass
        bus.WriteByteToReg(mpu9255, 0x37, 0x02)


	//how about traverse the entire i2c map for register values?
	//for li:=0; li<=0x7E; li++{
	//	lk,_:=bus.ReadByteFromReg(mpu9255, byte(li))
	//	if lk > 0 { fmt.Printf("Bus:%x, Address:%x on register %x value :  %x \n", 1, mpu9255, li, lk) }
	//}
	
	//sleep to wait for magcompass to wake up
	time.Sleep(100 * time.Millisecond)
	
	//then we should be seeing a new device @0x0C
	for li:=0; li<=0x15; li++{
        	lk,_:=bus.ReadByteFromReg(magcomp, byte(li))
		if lk >= 0 { fmt.Printf("Bus:%x, Address:%x on register %x value :  %x \n", 1, magcomp, li, lk) }
	}

	

}

package main

import "github.com/kidoman/embd"
import _ "github.com/kidoman/embd/host/all"
import "fmt"
//import "strconv"

func main(){
	fmt.Println("Initialize the mpu9255 modules...")

	for ii:=0; ii<128; ii++ {
		bus := embd.NewI2CBus(byte(ii))
		defer embd.CloseI2C()
		//how about traverse the entire i2c map for register values?
		for j:=0; j<=0x77; j++{
			for li:=0; li<0x7E; li++{
				lk,_:=bus.ReadByteFromReg(byte(j), byte(li))
				if lk > 0 { fmt.Printf("Bus:%x, Address:%x on register %x value value of %x \n",ii, j, li, lk) }
			}
		}

	}
	
}

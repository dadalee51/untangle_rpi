package main

import "github.com/kidoman/embd"
import _ "github.com/kidoman/embd/host/all"
import "fmt"
import "time"
import "sync"

func main(){
        fmt.Println("Initialize the mpu9255 modules...")
        const mpu9255 = 0x68
        const magcomp = 0x0C
        const magcomp_devid = 0x48
	var data [6]byte
	var wg sync.WaitGroup
	 
        bus := embd.NewI2CBus(1)
        defer embd.CloseI2C()

        //we want to send a power reset to 0x6B
        bus.WriteByteToReg(mpu9255, 0x6B, 0x00)

        //then set the bypass enabled for magnetic compass
        bus.WriteByteToReg(mpu9255, 0x37, 0x02)


        //how about traverse the entire i2c map for register values?
        //for li:=0; li<=0x7E; li++{
        //      lk,_:=bus.ReadByteFromReg(mpu9255, byte(li))
        //      if lk > 0 { fmt.Printf("Bus:%x, Address:%x on register %x value :  %x \n", 1, mpu9255, li, lk) }
        //}

        //sleep to wait for magcompass to wake up
        time.Sleep(100 * time.Millisecond)

        //then we should be seeing a new device @0x0C
        //for li:=0; li<=0x15; li++{
        //      lk,_:=bus.ReadByteFromReg(magcomp, byte(li))
        //      if lk >= 0 { fmt.Printf("Bus:%x, Address:%x on register %x value :  %x \n", 1, magcomp, li, lk) }
        //}

        if md,_ := bus.ReadByteFromReg(magcomp, 0x00); md != magcomp_devid {
                fmt.Errorf("magnetometer device id not matching 0x48")
		return
        }

        //then write to magnetic control 1 register, for continuous read
        bus.WriteByteToReg(magcomp, 0x0A, 0x02)

	wg.Add(1)

	//then start in a separate loop
	go func () {
		defer embd.CloseI2C()
		defer wg.Done()
		for { 
			st1,_ := bus.ReadByteFromReg(magcomp, 0x02) 
			//fmt.Println(st1)
			drdy := st1 & 0x02 >> 1
			//fmt.Println(drdy)
			switch drdy {
			case 1:
				//then read from register
				data[0],_ = bus.ReadByteFromReg(magcomp, 0x03)
				data[1],_ = bus.ReadByteFromReg(magcomp, 0x04)
				data[2],_ = bus.ReadByteFromReg(magcomp, 0x05)
				data[3],_ = bus.ReadByteFromReg(magcomp, 0x06)
				data[4],_ = bus.ReadByteFromReg(magcomp, 0x07)
				data[5],_ = bus.ReadByteFromReg(magcomp, 0x08)

				//then read ST2 to reset the read sequence
				bus.ReadByteFromReg(magcomp, 0x09)
				fmt.Print("data:")
				fmt.Println(data)
				//fmt.Printf("st2:%x", st2)
			case 0:
				//data not ready, do nothing
				//fmt.Println("data not ready")
				time.Sleep(100 * time.Millisecond)
			default:
				fmt.Println("unexpected state while trying to read magcompass")
				time.Sleep(100 * time.Millisecond)
			}
			time.Sleep(10 * time.Millisecond)
		}
	}()
	
	wg.Wait()
}


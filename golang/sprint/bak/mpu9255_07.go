package main

import (
	"fmt"
	"time"

	"github.com/kidoman/embd"
	_ "github.com/kidoman/embd/host/all"
)

func main() {
	fmt.Println("Initialize the mpu9255 modules...")
	//var data [6]byte
	//var wg sync.WaitGroup

	bus := embd.NewI2CBus(1)
	defer embd.CloseI2C()

	//we want to send a power reset to pwr_mgmt_1 0x6B
	bus.WriteByteToReg(mpu9255, pwr_mgmt_1, 0x40)
	time.Sleep(time.Millisecond * 50)
	//clear sleep bit and select clock source to PLL with Xaxis gyroscope reference
	bus.WriteByteToReg(mpu9255, pwr_mgmt_1, 0x01)
	bus.WriteByteToReg(mpu9255, gyro_cfg, 0x00)
	bus.WriteByteToReg(mpu9255, accel_cfg, 0x00)
	//then we setup the USER_CTRL register user_ctrl 0x6A to 0b11000000 to 0xC0 (7th is DMP enable??, 6th is FIFO )
	bus.WriteByteToReg(mpu9255, user_ctrl, 0xC0)

	fmt.Printf("Loading motion driver firmware to memory...")
	dmp_load_motion_driver_firmware(bus)
	dmp_set_orientation(
		inv_orientation_matrix_to_scalar(gyro_orientation), bus)

	var dmp_features uint16 = DMP_FEATURE_6X_LP_QUAT | DMP_FEATURE_SEND_RAW_ACCEL | DMP_FEATURE_SEND_CAL_GYRO | DMP_FEATURE_GYRO_CAL
	dmp_enable_feature(dmp_features, bus)
	dmp_set_fifo_rate(DEFAULT_MPU_HZ, bus)
	mpu_set_dmp_state(1, bus)

	//in this loop, we check the fifo for length, and read from the fifo.
	for {
		//fill the 12 length fifo data
		countFifo, err := bus.ReadWordFromReg(mpu9255, fifo_count_h)
		fmt.Printf("fifo is now: %v \n", countFifo)
		if err != nil {
			fmt.Println(err)
		}
		time.Sleep(time.Millisecond * 100)
		//also we want to send random test byte to 0x04 (arduino)
		//bus.WriteByteToReg(0x04, 0x00, byte(rand.Intn(180)))
	}

}

//utils----------------------------------------------------------------

func dmp_set_fifo_rate(rate uint16, bus embd.I2CBus) int {
	var regs_end = []uint8{DINAFE, DINAF2, DINAAB, 0xc4, DINAAA, DINAF1, DINADF, DINADF, 0xBB, 0xAF, DINADF, DINADF}
	var div uint16
	var tmp = []uint8{0, 0}

	if rate > DMP_SAMPLE_RATE {
		return -1
	}
	div = DMP_SAMPLE_RATE/rate - 1
	tmp[0] = uint8((div >> 8) & 0xFF)
	tmp[1] = uint8(div & 0xFF)
	mpu_block_write(D_0_22, 2, tmp, bus)
	mpu_block_write(CFG_6, 12, regs_end, bus)

	return 0
}

func inv_orientation_matrix_to_scalar(mtx []int8) uint16 {
	var scalar uint16

	/*
	   XYZ  010_001_000 Identity Matrix
	   XZY  001_010_000
	   YXZ  010_000_001
	   YZX  000_010_001
	   ZXY  001_000_010
	   ZYX  000_001_010
	*/

	scalar = inv_row_2_scale(mtx[0:3])
	scalar |= inv_row_2_scale(mtx[3:6]) << 3
	scalar |= inv_row_2_scale(mtx[6:9]) << 6

	return scalar
}

func inv_row_2_scale(row []int8) uint16 {
	var b uint16

	if row[0] > 0 {
		b = 0
	} else if row[0] < 0 {
		b = 4
	} else if row[1] > 0 {
		b = 1
	} else if row[1] < 0 {
		b = 5
	} else if row[2] > 0 {
		b = 2
	} else if row[2] < 0 {
		b = 6
	} else {
		b = 7 // error
	}
	return b
}

func dmp_enable_feature(mask uint16, bus embd.I2CBus) int {
	var tmp = []uint8{0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

	/* Set integration scale factor. */
	tmp[0] = uint8((GYRO_SF >> 24) & 0xFF)
	tmp[1] = uint8((GYRO_SF >> 16) & 0xFF)
	tmp[2] = uint8((GYRO_SF >> 8) & 0xFF)
	tmp[3] = uint8(GYRO_SF & 0xFF)
	mpu_block_write(D_0_104, 4, tmp, bus)

	/* Send sensor data to the FIFO. */
	tmp[0] = 0xA3
	if mask&DMP_FEATURE_SEND_RAW_ACCEL == 1 {
		tmp[1] = 0xC0
		tmp[2] = 0xC8
		tmp[3] = 0xC2
	} else {
		tmp[1] = 0xA3
		tmp[2] = 0xA3
		tmp[3] = 0xA3
	}
	if mask&DMP_FEATURE_SEND_ANY_GYRO == 1 {
		tmp[4] = 0xC4
		tmp[5] = 0xCC
		tmp[6] = 0xC6
	} else {
		tmp[4] = 0xA3
		tmp[5] = 0xA3
		tmp[6] = 0xA3
	}
	tmp[7] = 0xA3
	tmp[8] = 0xA3
	tmp[9] = 0xA3
	mpu_block_write(CFG_15, 10, tmp, bus)

	if mask&DMP_FEATURE_GYRO_CAL == 1 {
		dmp_enable_gyro_cal(1, bus)
	} else {
		dmp_enable_gyro_cal(0, bus)
	}

	if mask&DMP_FEATURE_SEND_ANY_GYRO == 1 {
		if mask&DMP_FEATURE_SEND_CAL_GYRO == 1 {
			tmp[0] = 0xB2
			tmp[1] = 0x8B
			tmp[2] = 0xB6
			tmp[3] = 0x9B
		} else {
			tmp[0] = DINAC0
			tmp[1] = DINA80
			tmp[2] = DINAC2
			tmp[3] = DINA90
		}
		mpu_block_write(CFG_GYRO_RAW_DATA, 4, tmp, bus)
	}

	if mask&DMP_FEATURE_LP_QUAT == 1 {
		dmp_enable_lp_quat(1, bus)
	} else {
		dmp_enable_lp_quat(0, bus)
	}

	if mask&DMP_FEATURE_6X_LP_QUAT == 1 {
		dmp_enable_6x_lp_quat(1, bus)
	} else {
		dmp_enable_6x_lp_quat(0, bus)
	}

	mpu_reset_fifo(bus)
	return 0
}

func dmp_load_motion_driver_firmware(bus embd.I2CBus) {
	//dmp length is 3062
	//dmp code
	//start address of DMP code memory 0x0400
	//sample rate is 200??

	mpu_write_dmp_mem(DMP_CODE_SIZE, progmem_start, bus)
	bus.WriteWordToReg(mpu9255, prgm_start_h, progmem_start)
}

//set dmp orientation
func dmp_set_orientation(orient uint16, bus embd.I2CBus) {
	var gyro_regs = []uint8{DINA4C, DINACD, DINA6C}
	var accel_regs = []uint8{DINA0C, DINAC9, DINA2C}

	var gyro_sign = []uint8{DINA36, DINA56, DINA76}
	var accel_sign = []uint8{DINA26, DINA46, DINA66}

	/* Chip-to-body, axes only. */
	mpu_block_write(FCFG_1, 3, gyro_regs, bus)
	mpu_block_write(FCFG_2, 3, accel_regs, bus)

	copy(gyro_regs, gyro_sign)
	copy(accel_regs, accel_sign)

	if (orient & 4) == 4 {
		gyro_regs[0] |= 1
		accel_regs[0] |= 1
	}
	if orient&0x20 == 0x20 {
		gyro_regs[1] |= 1
		accel_regs[1] |= 1
	}
	if orient&0x100 == 0x100 {
		gyro_regs[2] |= 1
		accel_regs[2] |= 1
	}

	/* Chip-to-body, sign only. */
	mpu_block_write(FCFG_3, 3, gyro_regs, bus)
	mpu_block_write(FCFG_7, 3, accel_regs, bus)
}

//block write with length support, this is the original write_mem function from C code.
func mpu_block_write(mem_addr uint16, length uint16, data []uint8, bus embd.I2CBus) int {

	if int(length) > len(data) {
		return -1
	}
	for ii := 0; ii < int(length); ii++ {
		bus.WriteWordToReg(mpu9255, bank_sel, uint16(ii))
		bus.WriteByteToReg(mpu9255, mem_r_w, data[ii])
		fmt.Printf("Writing to %x Selecting memory addr: %x , Write byte: %x \n", bank_sel, ii, data[ii])
	}
	return 0
}

func mpu_write_dmp_mem(length uint16, start_addr uint16, bus embd.I2CBus) {

	mpu_block_write(0, length, dmp_memory, bus)

	bus.WriteWordToReg(mpu9255, prgm_start_h, start_addr)
}

func dmp_enable_lp_quat(enable uint8, bus embd.I2CBus) int {
	var regs = []uint8{0, 0, 0, 0, 0}
	if enable == 1 {
		regs[0] = DINBC0
		regs[1] = DINBC2
		regs[2] = DINBC4
		regs[3] = DINBC6

	} else {
		regs = []uint8{0x8B, 0x8B, 0x8B, 0x8B}
	}
	mpu_block_write(CFG_LP_QUAT, 4, regs, bus)
	return mpu_reset_fifo(bus)
}

func dmp_enable_6x_lp_quat(enable uint8, bus embd.I2CBus) int {
	var regs = []uint8{0, 0, 0, 0}
	if enable == 1 {
		regs[0] = DINA20
		regs[1] = DINA28
		regs[2] = DINA30
		regs[3] = DINA38
	} else {
		regs = []uint8{0xA3, 0xA3, 0xA3, 0xA3}
	}
	mpu_block_write(CFG_8, 4, regs, bus)
	return mpu_reset_fifo(bus)
}

func mpu_reset_fifo(bus embd.I2CBus) int {
	var data uint8 = 0x00
	bus.WriteByteToReg(mpu9255, int_enable, data)
	bus.WriteByteToReg(mpu9255, fifo_en, data)
	bus.WriteByteToReg(mpu9255, user_ctrl, data)

	data = BIT_FIFO_RST | BIT_DMP_RST
	bus.WriteByteToReg(mpu9255, user_ctrl, data)

	time.Sleep(50 * time.Millisecond)
	data = BIT_DMP_EN | BIT_FIFO_EN
	bus.WriteByteToReg(mpu9255, int_enable, data)

	data = 0x00
	bus.WriteByteToReg(mpu9255, fifo_en, data)

	return 0
}

func dmp_enable_gyro_cal(enable uint8, bus embd.I2CBus) int {
	if enable == 1 {
		var regs = []uint8{0xb8, 0xaa, 0xb3, 0x8d, 0xb4, 0x98, 0x0d, 0x35, 0x5d}
		return mpu_block_write(CFG_MOTION_BIAS, 9, regs, bus)
	} else {
		var regs = []uint8{0xb8, 0xaa, 0xaa, 0xaa, 0xb0, 0x88, 0xc3, 0xc5, 0xc7}
		return mpu_block_write(CFG_MOTION_BIAS, 9, regs, bus)
	}
}

func mpu_set_dmp_state(enable uint8, bus embd.I2CBus) int {
	var tmp uint8

	if enable == 1 {
		/* Disable data ready interrupt. */
		//set_int_enable(0); //we are not using interrupts.
		/* Disable bypass mode. */
		//mpu_set_bypass(0)
		/* Keep constant sample rate, FIFO rate controlled by DMP. */
		//mpu_set_sample_rate(DMP_SAMPLE_RATE)
		bus.WriteByteToReg(mpu9255, rate_div, 1000/200-1)
		/* Remove FIFO elements. */
		tmp = 0
		//i2c_write(st.hw->addr, 0x23, 1, &tmp);
		bus.WriteByteToReg(mpu9255, 0x23, tmp)
		/* Enable DMP interrupt. */
		//set_int_enable(1);
		mpu_reset_fifo(bus)
	} else {
		/* Disable DMP interrupt. */
		//set_int_enable(0);
		/* Restore FIFO settings. */

		//i2c_write(st.hw->addr, 0x23, 1, &tmp);

		mpu_reset_fifo(bus)
	}
	return 0
}

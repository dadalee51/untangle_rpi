package main

import "github.com/kidoman/embd"
import "github.com/kidoman/embd/sensor/bmp180"
import _ "github.com/kidoman/embd/host/all"
import "fmt"

func main(){

	bus := embd.NewI2CBus(1)
	baro := bmp180.New(bus)

	temp, _ := baro.Temperature()
	altitude, _ := baro.Altitude()
	fmt.Println(temp)
	fmt.Printf("and altitude: %v", altitude)
	
}

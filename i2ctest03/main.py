#!/usr/bin/python

import Adafruit_BMP.BMP085 as BMP085
import smbus
import math
from qk import QuickKalman
import time
import serial, time, string
import roblib
import thread
import sys


#setup gyro
roblib.setup()

#GPS signal reading
ser = serial.Serial('/dev/ttyAMA0', 9600)

#setup barometer
bmp180 = BMP085.BMP085()

#setup mpu6050 6-axis
# Power management registers
power_mgmt_1 = 0x6b
power_mgmt_2 = 0x6c

def read_byte(adr):
    return bus.read_byte_data(address, adr)

def read_word(adr):
    high = bus.read_byte_data(address, adr)
    low = bus.read_byte_data(address, adr+1)
    val = (high << 8) + low
    return val

def read_word_2c(adr):
    val = read_word(adr)
    if (val >= 0x8000):
        return -((65535 - val) + 1)
    else:
        return val

def dist(a,b):
    return math.sqrt((a*a)+(b*b))

def get_y_rotation(x,y,z):
    radians = math.atan2(x, dist(y,z))
    return -math.degrees(radians)

def get_x_rotation(x,y,z):
    radians = math.atan2(y, dist(x,z))
    return math.degrees(radians)

bus = smbus.SMBus(1) # or bus = smbus.SMBus(1) for Revision 2 boards
address = 0x68       # This is the address value read via the i2cdetect command

# Now wake the 6050 up as it starts in sleep mode
bus.write_byte_data(address, power_mgmt_1, 0)

#setup Kalmans
#QuickKalman
qk_alt = QuickKalman(0.5, 0.5, 0.6)
qk_tmp = QuickKalman(0.1, 0.001, 0.05)
qk_rx = QuickKalman(0.005, 0.5, 0.5)
qk_ry = QuickKalman(0.005, 0.5, 0.5)
qk_rz = QuickKalman(0.005, 0.5, 0.5)

def gps_loop():
	while True:
		#GPS signal
		gpsdata=ser.readline()
		if gpsdata.startswith ('$'):
			print(string.strip(gpsdata))
		#time.sleep(0.1)

def bmp_loop():
	while True:		
		bmpalt = bmp180.read_altitude()
		bmptmp = bmp180.read_temperature()
		print("bmp:" + str(bmpalt) + ',' + str(bmptmp))
		#time.sleep(0.1)

def gyro_loop():
	while True:
		#gyro_loop
		gylp = roblib.loop()
		if gylp is not None :
			for a in gylp:
				print a,
			print ""
		#time.sleep(0.1)
#dispatch three threads running wild!
try:
	thread.start_new_thread(gps_loop,())
	thread.start_new_thread(bmp_loop,())
	thread.start_new_thread(gyro_loop, ())
	pass
except TypeError as e:
	print "something wrong in dispatch func"
	a = e
	print( a )

while 1:
	pass

ser.close()

#!/usr/bin/python

import Adafruit_BMP.BMP085 as BMP085
import smbus
import math
from qk import QuickKalman
import time
import serial, time, string
import roblib
import thread
import sys
from comp_brng import getCompBearing
import numpy

#setup gyro
roblib.setup()

#GPS signal reading
ser = serial.Serial('/dev/ttyAMA0', 9600)

#setup barometer
bmp180 = BMP085.BMP085()

#setup mpu6050 6-axis
# Power management registers
power_mgmt_1 = 0x6b
power_mgmt_2 = 0x6c

def read_byte(adr):
    return bus.read_byte_data(address, adr)

def read_word(adr):
    high = bus.read_byte_data(address, adr)
    low = bus.read_byte_data(address, adr+1)
    val = (high << 8) + low
    return val

def read_word_2c(adr):
    val = read_word(adr)
    if (val >= 0x8000):
        return -((65535 - val) + 1)
    else:
        return val

def dist(a,b):
    return math.sqrt((a*a)+(b*b))

def get_y_rotation(x,y,z):
    radians = math.atan2(x, dist(y,z))
    return -math.degrees(radians)

def get_x_rotation(x,y,z):
    radians = math.atan2(y, dist(x,z))
    return math.degrees(radians)

bus = smbus.SMBus(1) # or bus = smbus.SMBus(1) for Revision 2 boards
address = 0x68       # This is the address value read via the i2cdetect command

# Now wake the 6050 up as it starts in sleep mode
bus.write_byte_data(address, power_mgmt_1, 0)

#setup Kalmans
#QuickKalman
qk_alt = QuickKalman(0.5, 0.5, 0.6)
qk_tmp = QuickKalman(0.1, 0.001, 0.05)
qk_gps_lat = QuickKalman(0.0001, 0.0001, 0.1)
qk_gps_lng = QuickKalman(0.0001, 0.0001, 0.1)
qk_gps_lat2 = QuickKalman(0.01, 0.01, 0.001) #second degree filter
qk_gps_lng2 = QuickKalman(0.01, 0.01, 0.001) #second degree filter
gps_round = 3
def gps_loop():
	last_pos = (0.0,0.0)
	while True:
		#GPS signal
		gpsdata=ser.readline()
		if gpsdata.startswith ('$GPRMC'):
			#then we parse this line
			#print(string.strip(gpsdata))
			gpsdata = string.strip(gpsdata)
			lat = gpsdata.split(",")[3]
			latd = gpsdata.split(",")[4] #if S then its negative
			if latd == "S" :
				latd ="-"
			else:
				latd = ""
			lng = gpsdata.split(",")[5]
			lngd = gpsdata.split(",")[6] #if W then negative
			if lngd == "W" :
				lngd = "-"
			else:
				lngd = ""
			latDD = lat[0:2]
			latMMMMM = lat[2:10]
			lngDD = lng[0:3]
			lngMMMMM = lng[3:11]
			#process qk
			lat_qk = qk_gps_lat.next(float(latMMMMM))
			lng_qk = qk_gps_lng.next(float(lngMMMMM))
			
			#output rounding
			rlat_qk = round(lat_qk, gps_round)
			rlng_qk = round(lng_qk, gps_round)
			
			#second tier damper
			lat_qk2 = qk_gps_lat2.next(rlat_qk)
			lng_qk2 = qk_gps_lng2.next(rlng_qk)
			
			#round again
			rlat_qk2 = round(lat_qk2, gps_round)
			rlng_qk2 = round(lng_qk2, gps_round)
			
			#compose degrees decimal
			dlat_qk2 = rlat_qk2/60 + float(latDD)
			dlng_qk2 = rlng_qk2/60 + float(lngDD)
			
			if latd=="-":
				dlat_qk2*=-1
			
			if lngd=="-":	
				dlng_qk2*=-1
			this_pos = (dlat_qk2, dlng_qk2)	
			#calculate compass bearing
			bearing = getCompBearing(last_pos, this_pos)
			
			#from the degrees we are also able to calculate the distance drifted.
			#a degree of latitude is 110947.2m
			#a degree of longitude is 87843.36m
			drift_pos = numpy.multiply(tuple(numpy.subtract(this_pos, last_pos)), (110947.2,87843.36))
						
			#print str(latd) + latDD + " " + latMMMMM + ", " + str(lngd) + " " + lngDD + " " + lngMMMMM 
			#print str(latd) + latDD + " " + str(lat_qk) + ", " + str(lngd) + " " + lngDD + " " + str(lng_qk)
			#print str(latd) + latDD + " " + str(rlat_qk) + ", " + str(lngd) + " " + lngDD + " " + str(rlng_qk)
			
			print str(latd) + latDD + " " + str(rlat_qk2) + ", " + str(lngd) + " " + lngDD + " " + str(rlng_qk2) + " bearing: " + str(bearing) + ", Degrees:" + str(dlat_qk2) + "," + str(dlng_qk2) + ", drift(meters) = " + str(drift_pos)
			#the above signal can be directly searched by google maps.
		
			#however we still need to have a compass in order to know which direction we are facing in order to counter-move...
			
			last_pos = this_pos
def bmp_loop():
	while True:		
		bmpalt = bmp180.read_altitude()
		bmptmp = bmp180.read_temperature()
		print("bmp:" + str(bmpalt) + ',' + str(bmptmp))
		#time.sleep(0.1)

def gyro_loop():
	while True:
		#gyro_loop
		gylp = roblib.loop()
		if gylp is not None :
			for a in gylp:
				print a,
			print ""
		#time.sleep(0.1)
#dispatch three threads running wild!
try:
	thread.start_new_thread(gps_loop,())
	thread.start_new_thread(bmp_loop,())
	thread.start_new_thread(gyro_loop, ())
	pass
except TypeError as e:
	print "something wrong in dispatch func"
	a = e
	print( a )

while 1:
	pass

ser.close()

#include <Servo.h>
#include <Wire.h>

Servo d10;  // create servo object to control a servo 
Servo d5;
Servo d6;
Servo d9;

int pos = 0;  

void setup()
{
  Serial.begin(9600);           // start serial for output
  Serial.println("Hello World!");
  //changing he pwm freq.
    
  d10.attach(5);
  d5.attach(10);
  d6.attach(9);
  d9.attach(6);
  
  Wire.begin(4);                // join i2c bus with address #4
  Wire.onReceive(receiveEvent); // register event
  Wire.onRequest(sendEvent);    //setup sentEvent callback
  
  //then we change the PWM divisor:
  setPwmFrequency(5, 256);
  setPwmFrequency(9, 128);


  int maxPos = 90;
  int minPos = 25;

  for(pos = minPos; pos < maxPos; pos += 1)
    writeAll(pos, 20);
  for(pos = maxPos; pos>=minPos; pos-=1)                                
    writeAll(pos, 20);
  writeAll(64,10);
  newDelay(1000);
  writeAll(pos,10); // turn off.
}

void writeAll(int pos, int dly){
  Serial.println("Start writing:");
  Serial.print(pos);
  d10.write(pos);              // tell servo to go to position in variable 'pos' 
  //d3.write(pos);
  d6.write(pos);
  d5.write(pos);
  d9.write(pos);
  newDelay(dly); 
  Serial.println("end ");
  
}

void loop()
{
  //Serial.println("Pass");
  newDelay(1000); 
  Wire.endTransmission(0);
  
}
//----------------------------------------------I2C
// function that executes whenever data is received from master
// this function is registered as an event, see setup()
void receiveEvent(int howMany)
{
  while(1 < Wire.available()) // loop through all but the last
  {
    char c = Wire.read(); // receive byte as a character
    Serial.print(c);         // print the character
  }
  int x = Wire.read();    // receive byte as an integer
  Serial.println(x);         // print the integer
  pos = x;
}

void sendEvent(){
  Wire.write(pos);
  newDelay(100);
}

//=============================================end I2C

void setPwmFrequency(int pin, int divisor) {
  byte mode;
  if(pin == 5 || pin == 6 || pin == 9 || pin == 10) {
    switch(divisor) {
      case 1: mode = 0x01; break;
      case 8: mode = 0x02; break;
      case 64: mode = 0x03; break;
      case 256: mode = 0x04; break;
      case 1024: mode = 0x05; break;
      default: return;
    }
    if(pin == 5 || pin == 6) {
      TCCR0B = TCCR0B & 0b11111000 | mode;
    } else {
      TCCR1B = TCCR1B & 0b11111000 | mode;
    }
  } else if(pin == 3 || pin == 11) {
    switch(divisor) {
      case 1: mode = 0x01; break;
      case 8: mode = 0x02; break;
      case 32: mode = 0x03; break;
      case 64: mode = 0x04; break;
      case 128: mode = 0x05; break;
      case 256: mode = 0x06; break;
      case 1024: mode = 0x07; break;
      default: return;
    }
    TCCR2B = TCCR2B & 0b11111000 | mode;
  }
}

//this is the more accurate delay from the divisor changes.
void newDelay(unsigned long inp){
  delay(inp / 4);
}

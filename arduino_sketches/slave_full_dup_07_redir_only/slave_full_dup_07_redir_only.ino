#define DEBUG_MON

//DEBUG_ALL may require larger delay value from server.py.
#ifdef DEBUG_ALL
 #define dbgaPln(x)  Serial.println (x)
 #define dbgaP(x)  Serial.print (x)
 #define dbgPln(x)  Serial.println (x)
 #define dbgP(x)  Serial.print (x)
 #define dbgSBegin(x) Serial.begin(x)
#endif
#ifdef DEBUG_MON
 #define dbgaPln(x)
 #define dbgaP(x)
 #define dbgPln(x)  Serial.println (x)
 #define dbgP(x)  Serial.print (x)
 #define dbgSBegin(x) Serial.begin(x)
#else
 #define dbgaPln(x)
 #define dbgaP(x)
 #define dbgPln(x)
 #define dbgP(x)
 #define dbgSBegin(x)
#endif

#include <Servo.h>
#include <Wire.h>

const int pingPin = 3;
Servo d10, d5, d6,d9;
int pos = 0; //global throttle.
unsigned int height = 0; //global height measured.2 bytes
unsigned long uptime = 0; //uptime, 4 * millis()
float altitude = 0.0; //target to achieve. 4 bytes
char buff[16]; //our receive buffer, we expect 'uTFF', where the first two byte are checkers, next two byte are data.
char outf[16]; //output buffer, max 16 bytes
boolean recFlag = false;  //will only process input buffer if actually receives anything from wire.
//@TODO: no self correction mech yet.

void setup()
{
  Wire.begin(4);                // join i2c bus with address #4
  Wire.onReceive(receiveEvent); // register event
  Wire.onRequest(requestEvent); // register event
  dbgSBegin(9600);
 
  //attach servo mapping
  d10.attach(5);
  d5.attach(10);
  d6.attach(9);
  d9.attach(6);

  //then we change the PWM divisor:
  setPwmFrequency(5, 256);
  setPwmFrequency(9, 128);
  
  //init servo
  int maxPos = 90;
  int minPos = 25;
  for(pos = minPos; pos < maxPos; pos += 1)
    writeAll(pos, 20);
  for(pos = maxPos; pos>=minPos; pos-=1)                                
    writeAll(pos, 20);
  writeAll(64,10);
  newDelay(100);
  writeAll(pos,10); // turn off, when pos=0
}

//servo functions
void writeAll(int pos, int dly){
  dbgaPln("Start writing:");
  dbgaP(pos);
  d10.write(pos);              // tell servo to go to position in variable 'pos' 
  //d3.write(pos);
  d6.write(pos);
  d5.write(pos);
  d9.write(pos);
  newDelay(dly); 
  dbgaPln("end ");
}


//main loop
void loop()
{
  ping(pingPin, 1); //automatically pings for height, needed for self control.  
  //newDelay(100);
}



// function that executes whenever data is received from master
// this function is registered as an event, see setup()
void receiveEvent(int howMany)
{
  int inIndex = 0;
  
  while(0 < Wire.available()) // loop through all but the last
  {
    char c = Wire.read(); // receive byte as a character
    recFlag = true;
    if(inIndex>1){
      buff[inIndex-2]= c;
      dbgaP(inIndex);         // print the character
      dbgaP(":");
      dbgaP(c);
      dbgaP(", ");
    }
    inIndex++;
    
    if (inIndex > 17)inIndex=0;
  }
  
  //do nothing if recFlag is false
  if(!recFlag)return;
  
  //process the checkers, 
  if (buff[0]=='u'){ //'u' requests are single commands, maximum of one return value.
    switch(buff[1]){
      case 'H': //report Height @ultrasonic
        dbgPln("Server: Rep Hht");
        pack_char('h', outf, 0); //indicate from arduino, height only
        pack_int(height, outf, 1);
        break;
      case 'C': //report Time Left
        dbgPln("Server: Rep Tme"); 
        uptime = millis() * 4;
        pack_char('u', outf, 0); //indicate from arduino, uptime only
        pack_long(uptime, outf, 3);
        break;
      case 'R': //report all
        dbgPln("Server: Rep All");
        pack_char('r', outf, 0); //indicate from arduino, report all
        pack_int(height, outf, 1);
        uptime = millis() * 4;
        dbgPln(uptime);
        pack_long(uptime, outf, 3);
        //dbgPln(outf);
        break;
      default:
        break;
      
    }
  }else if(buff[0]=='a'){ //'a' requests are action commands, they request movement directions, throttle, altitude...
      switch(buff[1]){
        case 'T': //Throttle
          dbgP("T:");
          dbgP(buff[2] + 0);
          pos = buff[2] + 0; //only lsb chosen, since the value wont exceed more than 100..
          break;
        case 'D': //Direction of travel, along with speed of travel.
          break;
        case 'A': //Altitude to achieve. Note this will override the Throttle command and send the UAV into auto alt mode.
          //dbgP("A:");
          //dbgP(buff[2] + 0);
          //received 4 bytes, 
          memcpy(&altitude, &buff[2], sizeof(altitude));
          dbgP(" A:");
          dbgP(altitude);
          break;
        case 'S': //emergency STOP, power down everything and reset.
          dbgPln("STOP!");
          pos = 0;
          altitude = 0;
          memset(buff,0,sizeof(buff));
          memset(outf,0,sizeof(outf));
          break;
        default:
          break; 
    }
  }
  
  //all of our actions will clear the flag and buffer.
  memset(buff,0,sizeof(buff));
  recFlag = false;
}

//request from master, we send it back...
void requestEvent(){
  Wire.write((byte*)outf, 16);
  memset(outf,0,sizeof(outf));
}

void setPwmFrequency(int pin, int divisor) {
  byte mode;
  if(pin == 5 || pin == 6 || pin == 9 || pin == 10) {
    switch(divisor) {
      case 1: mode = 0x01; break;
      case 8: mode = 0x02; break;
      case 64: mode = 0x03; break;
      case 256: mode = 0x04; break;
      case 1024: mode = 0x05; break;
      default: return;
    }
    if(pin == 5 || pin == 6) {
      TCCR0B = TCCR0B & 0b11111000 | mode;
    } else {
      TCCR1B = TCCR1B & 0b11111000 | mode;
    }
  } else if(pin == 3 || pin == 11) {
    switch(divisor) {
      case 1: mode = 0x01; break;
      case 8: mode = 0x02; break;
      case 32: mode = 0x03; break;
      case 64: mode = 0x04; break;
      case 128: mode = 0x05; break;
      case 256: mode = 0x06; break;
      case 1024: mode = 0x07; break;
      default: return;
    }
    TCCR2B = TCCR2B & 0b11111000 | mode;
  }
}

//this is the more accurate delay from the divisor changes.
void newDelay(unsigned long inp){
  delay(inp / 4);
}


//ping func
void ping(int pinNum, int delayTime)
{
  long duration, cm;
  newDelay(delayTime);
  pinMode(pinNum, OUTPUT);
  digitalWrite(pinNum, LOW);
  newDelay(2);
  digitalWrite(pinNum, HIGH);
  newDelay(5);
  digitalWrite(pinNum, LOW);
  pinMode(pinNum, INPUT);
  duration = pulseIn(pinNum, HIGH);
  cm = duration / 74 * 3 / 2 ;
  height = cm;
  dbgP(cm);
  dbgP("cm");
  dbgPln();
}

//int is 2 bytes. 
void pack_int(unsigned int n, char* c, int idx){
  memcpy(&c[idx], &n, sizeof(unsigned int));
}

//long is 4 bytes
void pack_long(unsigned long n, char* c, int idx){
  memcpy(&c[idx], &n, sizeof(unsigned long));
}

//char is 1 byte
void pack_char(unsigned char n, char* c, int idx){
  memcpy(&c[idx], &n, sizeof(char));
}

#define DEBUG_MON

//DEBUG_ALL may require larger delay value from server.py.
#ifdef DEBUG_ALL
 #define dbgaPln(x)  Serial.println (x)
 #define dbgaP(x)  Serial.print (x)
 #define dbgPln(x)  Serial.println (x)
 #define dbgP(x)  Serial.print (x)
 #define dbgSBegin(x) Serial.begin(x)
#endif
#ifdef DEBUG_MON
 #define dbgaPln(x)
 #define dbgaP(x)
 #define dbgPln(x)  Serial.println (x)
 #define dbgP(x)  Serial.print (x)
 #define dbgSBegin(x) Serial.begin(x)
#else
 #define dbgaPln(x)
 #define dbgaP(x)
 #define dbgPln(x)
 #define dbgP(x)
 #define dbgSBegin(x)
#endif

#include <Servo.h>
#include <Wire.h>
#include "I2Cdev.h"
#include "MPU9150_9Axis_MotionApps41.h"

//#define OUTPUT_READABLE_QUATERNION
//#define OUTPUT_READABLE_EULER
#define OUTPUT_READABLE_YAWPITCHROLL
//#define OUTPUT_READABLE_REALACCEL
//#define OUTPUT_READABLE_WORLDACCELvol
//#define LED_PIN 13
//bool blinkState = false;
MPU9150 mpu;
// MPU control/status vars
volatile bool buffReady = false; // singal buff is ready to read.
bool dmpReady = false;  // set true if DMP init was successful
uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU
uint8_t devStatus;      // return status after each device operation (0 = success, !0 = error)
uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[64]; // FIFO storage buffer

// orientation/motion vars
Quaternion q;           // [w, x, y, z]         quaternion container
VectorInt16 aa;         // [x, y, z]            accel sensor measurements
VectorInt16 aaReal;     // [x, y, z]            gravity-free accel sensor measurements
VectorInt16 aaWorld;    // [x, y, z]            world-frame accel sensor measurements
VectorFloat gravity;    // [x, y, z]            gravity vector
float euler[3];         // [psi, theta, phi]    Euler angle container
float ypr[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector
Servo d10, d5, d6,d9;
int pos = 0; //global throttle.
int state = -1; //armed
char buff[16]; //our receive buffer, we expect 'uTFF', where the first two byte are checkers, next two byte are data.
char outf[16]; //output buffer, max 16 bytes

//inttrupt
volatile bool mpuInterrupt = false;     // indicates whether MPU interrupt pin has gone high
void dmpDataReady() {
    mpuInterrupt = true;
}

//interrupt2
void receiveEvent(int howMany)
{
  int inIndex = 0;
  while(0 < Wire.available()){
    char c = Wire.read(); // receive byte as a character
    if(inIndex>1){
      buff[inIndex-2]= c;
      dbgaP(inIndex);         // print the character
      dbgaP(":");
      dbgaP(c);
      dbgaP(", ");
    }
    inIndex++;
    if (inIndex > 17)inIndex=0;
  }
  buffReady = true;
}

//request from master, we send it back...
void requestEvent(){
  for(int i=0;i<16;i++){
    Wire.write(outf[i]);
  }
}

void setup()
{
  Wire.begin(4);                // join i2c bus with address #4

  dbgSBegin(9600);
 
  mpu.initialize();
  devStatus = mpu.dmpInitialize();

  //mpu.setXGyroOffset(220);
  //mpu.setYGyroOffset(76);
  //mpu.setZGyroOffset(-85);
  //mpu.setZAccelOffset(1788); // 1688 factory default for my test chip

  if (devStatus == 0) {
    // turn on the DMP, now that it's ready
    dbgPln("Enabling DMP...");
    mpu.setDMPEnabled(true);
    
    // enable Arduino interrupt detection
    dbgPln("Enabling interrupt...");
    attachInterrupt(0, dmpDataReady, RISING);
    mpuIntStatus = mpu.getIntStatus();
    
    // set our DMP Ready flag so the main loop() function knows it's okay to use it
    dbgPln("DMP ready.");
    dmpReady = true;
    
    // get expected DMP packet size for later comparison
    packetSize = mpu.dmpGetFIFOPacketSize();
  } else {
    // ERROR!
    // 1 = initial memory load failed
    // 2 = DMP configuration updates failed
    // (if it's going to break, usually the code will be 1)
    dbgP("DMP Initialization failed (code ");
    dbgP(devStatus);
    dbgP(")");
  }
  
  // configure LED for output
  //pinMode(LED_PIN, OUTPUT);
  
  mpu.setRate(1999);
  
  //attach servo mapping
  d10.attach(5);
  d5.attach(10);
  d6.attach(9);
  d9.attach(6);

  //then we change the PWM divisor:
  //setPwmFrequency(5, 256);
  //setPwmFrequency(9, 128);
  
  //init servo
  int maxPos = 90;
  int minPos = 25;
  for(pos = minPos; pos < maxPos; pos += 1)
    writeAll(pos, 10);
  for(pos = maxPos; pos>=minPos; pos-=1)                                
    writeAll(pos, 10);
  writeAll(64,10);
  newDelay(200);
  writeAll(pos,10); // turn off, when pos=0
  
  //register the i2c event handlers
  Wire.onReceive(receiveEvent); // register event
  //Wire.onRequest(requestEvent); // register event
}

//servo functions
void writeAll(int pos, int dly){
  dbgaPln("Start writing:");
  dbgaP(pos);
  d10.write(pos);              // tell servo to go to position in variable 'pos' 
  //d3.write(pos);
  d6.write(pos);
  d5.write(pos);
  d9.write(pos);
  newDelay(dly); 
  dbgaPln("end ");
}


//main loop
void loop()
{
  //writeAll(pos,10); // turn off, when pos=0
  newDelay(5);
  
  if(dmpReady){
    mpuInterrupt = false;
    mpuIntStatus = mpu.getIntStatus();
    fifoCount = mpu.getFIFOCount();
    // check for overflow (this should never happen unless our code is too inefficient)
    if ((mpuIntStatus & 0x10) || fifoCount == 1024) {
      // reset so we can continue cleanly
      mpu.resetFIFO();
      dbgaPln("FIFO overflow!");

    // otherwise, check for DMP data ready interrupt (this should happen frequently)
    } else if (mpuIntStatus & 0x02) {
      // wait for correct available data length, should be a VERY short wait
      while (fifoCount < packetSize) fifoCount = mpu.getFIFOCount();
      
      // read a packet from FIFO
      mpu.getFIFOBytes(fifoBuffer, packetSize);
      // track FIFO count here in case there is > 1 packet available
      // (this lets us immediately read more without waiting for an interrupt)
      fifoCount -= packetSize;
      
      #ifdef OUTPUT_READABLE_QUATERNION
        // display quaternion values in easy matrix form: w x y z
        mpu.dmpGetQuaternion(&q, fifoBuffer);
        dbgP("quat\t");
        dbgP(q.w);
        dbgP("\t");
        dbgP(q.x);
        dbgP("\t");
        dbgP(q.y);
        dbgP("\t");
        dbgPln(q.z);
      #endif
      
      #ifdef OUTPUT_READABLE_EULER
        // display Euler angles in degrees
        mpu.dmpGetQuaternion(&q, fifoBuffer);
        mpu.dmpGetEuler(euler, &q);
        dbgP("euler\t");
        dbgP(euler[0] * 180/M_PI);
        dbgP("\t");
        dbgP(euler[1] * 180/M_PI);
        dbgP("\t");
        dbgPln(euler[2] * 180/M_PI);
      #endif
      
      #ifdef OUTPUT_READABLE_YAWPITCHROLL
        // display Euler angles in degrees
        mpu.dmpGetQuaternion(&q, fifoBuffer);
        mpu.dmpGetGravity(&gravity, &q);
        mpu.dmpGetYawPitchRoll(ypr, &q, &gravity);
        dbgP("ypr\t");
        dbgP(ypr[0] * 180/M_PI);
        dbgP("\t");
        dbgP(ypr[1] * 180/M_PI);
        dbgP("\t");
        dbgPln(ypr[2] * 180/M_PI);
      #endif
      
      #ifdef OUTPUT_READABLE_REALACCEL
        // display real acceleration, adjusted to remove gravity
        mpu.dmpGetQuaternion(&q, fifoBuffer);
        mpu.dmpGetAccel(&aa, fifoBuffer);
        mpu.dmpGetGravity(&gravity, &q);
        mpu.dmpGetLinearAccel(&aaReal, &aa, &gravity);
        dbgP("areal\t");
        dbgP(aaReal.x);
        dbgP("\t");
        dbgP(aaReal.y);
        dbgP("\t");
        dbgPln(aaReal.z);
      #endif
      
      #ifdef OUTPUT_READABLE_WORLDACCEL
        // display initial world-frame acceleration, adjusted to remove gravity
        // and rotated based on known orientation from quaternion
        mpu.dmpGetQuaternion(&q, fifoBuffer);
        mpu.dmpGetAccel(&aa, fifoBuffer);
        mpu.dmpGetGravity(&gravity, &q);
        mpu.dmpGetLinearAccel(&aaReal, &aa, &gravity);
        mpu.dmpGetLinearAccelInWorld(&aaWorld, &aaReal, &q);
        dbgP("aworld\t");
        dbgP(aaWorld.x);
        dbgP("\t");
        dbgP(aaWorld.y);
        dbgP("\t");
        dbgPln(aaWorld.z);
      #endif
    }
  }//end if dmp...
  
  
  //buff ready map..
  if(buffReady){
      checkMapAction();
      buffReady=false;
      //Wire.endTransmission(false);
  }
}

//checkerMethod
void checkMapAction(){
  //process the checkers, 
  if (buff[0]=='u'){ //'u' requests are single commands, maximum of one return value.
    switch(buff[1]){
      case 'A': //report Altitude
        dbgPln("AltRep:");
        break;
      case 'G': //report GPS
        break;
      case 'H': //report Height
        break;
      case 'B': //report bearing
        break;
      case 'C': //report Time Left
        break;
      case 'R': //report all
        dbgPln("RepAll");
        break;
      default:
        break;
      
    }
  }else if(buff[0]=='a'){ //'a' requests are action commands, they request movement directions, throttle, altitude...
      switch(buff[1]){
        case 'T': //Throttle
          dbgP("T:");
          dbgP(buff[2] + 0);
          pos = buff[2] + 0;
          break;
        case 'D': //Direction of travel, along with speed of travel.
          break;
        case 'A': //Altitude to achieve. Note this will override the Throttle command and send the UAV into auto alt mode.
          dbgP("A:");
          dbgP(buff[2] + 0);
          break;
        case 'S': //emergency STOP, power down everything and reset.
          dbgPln("STOP!");
          break;
        default:
          break; 
    }
  }
}


void setPwmFrequency(int pin, int divisor) {
  byte mode;
  if(pin == 5 || pin == 6 || pin == 9 || pin == 10) {
    switch(divisor) {
      case 1: mode = 0x01; break;
      case 8: mode = 0x02; break;
      case 64: mode = 0x03; break;
      case 256: mode = 0x04; break;
      case 1024: mode = 0x05; break;
      default: return;
    }
    if(pin == 5 || pin == 6) {
      TCCR0B = TCCR0B & 0b11111000 | mode;
    } else {
      TCCR1B = TCCR1B & 0b11111000 | mode;
    }
  } else if(pin == 3 || pin == 11) {
    switch(divisor) {
      case 1: mode = 0x01; break;
      case 8: mode = 0x02; break;
      case 32: mode = 0x03; break;
      case 64: mode = 0x04; break;
      case 128: mode = 0x05; break;
      case 256: mode = 0x06; break;
      case 1024: mode = 0x07; break;
      default: return;
    }
    TCCR2B = TCCR2B & 0b11111000 | mode;
  }
}

//this is the more accurate delay from the divisor changes.
void newDelay(unsigned long inp){
  delay(inp / 4);
}


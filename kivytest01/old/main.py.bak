import atexit
import json
import sys
import socket
import struct
import time
import mapview
import os
from os.path import join, dirname, normpath
from Queue import Queue
from threading import Thread, Lock,Condition
from kivy.app import App
from kivy.base import runTouchApp
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.uix.widget import Widget
from kivy.properties import NumericProperty, ObjectProperty,StringProperty
from kivy.uix.textinput import TextInput
from kivy.uix.tabbedpanel import TabbedPanel

if __name__ == '__main__' and __package__ is None:
    from os import sys, path
    sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

kwargs = {}
if len(sys.argv) > 1:
    kwargs["map_source"] = MapSource(url=sys.argv[1], attribution="")

#global methods

@atexit.register
def saveToConfig():
	print "Shutting down uav controller, saving configuration."
	configFN = 'uav_config.json'
	with open(configFN, 'w+') as f:
		global configUAV
		#save the ids to configUAV and write to file.
		configUAV['u_uav_ip'] = root.ids.u_set.ids.u_uav_ip.text
		configUAV['u_uav_port'] = root.ids.u_set.ids.u_uav_port.text
		#...more...
		json.dump(configUAV, f)
		f.close()

def loadFromConfig():
	global configUAV
	configFN = 'uav_config.json'
	if not os.path.exists(configFN):
		f = open(configFN, 'w+')
		f.close()
	try:
		f = open(configFN, 'r+')
		configUAV = json.load(f)
	except ValueError:
		print "no data in configUAV at this time, its okay.", sys.exc_info()[0]
	except IOError:
		print "something wrong trying to open/create config file, sys exit."
		sys.exit(1)
	else:
		print "Config file loaded with error: " , sys.exc_info()[0]
	#maps configUAV to ids
	if configUAV:
		if configUAV['u_uav_ip']:
			root.ids.u_set.ids.u_uav_ip.text = configUAV['u_uav_ip']
			root.ids.u_set.ids.u_uav_port.text = configUAV['u_uav_port']
	#do this regardless.
	root.ids.u_cc.ids.u_uav_ip = root.ids.u_set.ids.u_uav_ip 	
	root.ids.u_cc.ids.u_uav_port = root.ids.u_set.ids.u_uav_port

#class defs
count_live_threads = 0;


#Controller will have a socket client to connect to UAV.
class Controller(Widget):
	sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	sock_in = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	server_address = None
	server_in_addr = None
	uav_gps_lat_kal = None
	uav_gps_lon_kal = None
	q = Queue()

	def __init__(self, **kwargs):
		super(Controller, self).__init__(**kwargs)
		Clock.schedule_once(self.start_socket, 0)
		Clock.schedule_once(self.post_init, 0)
		Clock.schedule_interval(self.start_read_server_thread, 0.01) #read server every 1 s

	def post_init(self, val):
		#print "current source is:",root.ids.u_cc.ids.u_mapmrk.source
		root.ids.u_cc.ids.u_mapmrk_home.source=join(dirname(__file__), "mapview", "icons", "home.png")

	def start_read_server_thread(self, val):
		t1 = Thread(target=self.read_server, args=(self.q,))
		t1.daemon = True
		t1.start()

	def read_server(self,q):
		global count_live_threads
		count_live_threads+=1
		if count_live_threads > 1 :
			#print "too much live thread, suicide!"
			count_live_threads-=1
			#print "count:" , count_live_threads
			return
		try:
			#print "in readSock, trying recvfrom..."
			data, addr = self.sock_in.recvfrom(16)
			if data:
				id_byte = struct.unpack("c",data[0:1])[0]
				if id_byte in ('a', 'h', 'r'):
					uav_height = struct.unpack("H",data[1:3])[0]
					uav_uptime = struct.unpack("L",data[3:7])[0]
					root.ids.u_ifp.ids.u_info1.text = str( \
						"UAV Height: " + str(uav_height) + " cm\n" +\
						"UAV Uptime: " + f_uptime(uav_uptime/1000) + " sec\n")
				if id_byte in ('g',):
					self.uav_gps_lat_kal = struct.unpack("f",data[1:5])[0]
					self.uav_gps_lon_kal = struct.unpack("f",data[5:9])[0]
					#print "lat:", self.uav_gps_lat_kal
					#print "lon:", self.uav_gps_lon_kal
					root.ids.u_cc.ids.u_mapmrk.lat=self.uav_gps_lat_kal
					root.ids.u_cc.ids.u_mapmrk.lon=self.uav_gps_lon_kal
					#print root.ids.u_cc.ids.u_sticky_swtch.active
					if root.ids.u_cc.ids.u_sticky_swtch.active :
						root.ids.u_cc.ids.u_mapvew.center_on(self.uav_gps_lat_kal, self.uav_gps_lon_kal)
					root.ids.u_ifp.ids.u_info2.text = str( \
						"UAV Latitude: " + str(self.uav_gps_lat_kal) + "\n" +\
						"UAV Longitude: " + str(self.uav_gps_lon_kal) + "\n")
				if id_byte in ('b',):
					self.uav_bmp_alt = struct.unpack("f",data[1:5])[0]
					self.uav_bmp_tmp = struct.unpack("f",data[5:9])[0]
					root.ids.u_ifp.ids.u_info3.text = str( \
						"UAV Barometer Altitude: " + str(self.uav_bmp_alt) + " \n" +\
						"UAV Barometer Temperature: " + str(self.uav_bmp_tmp) + " \n")
				if id_byte in ('y',):
					self.uav_gyro_yaw = struct.unpack("f",data[1:5])[0]
					self.uav_gyro_pit = struct.unpack("f",data[5:9])[0]
					self.uav_gyro_rol = struct.unpack("f",data[9:13])[0]
					root.ids.u_ifp.ids.u_info4.text = str( \
						"UAV IMU Yaw: " + str(self.uav_gyro_yaw) + " \n" +\
						"UAV IMU Pitch: " + str(self.uav_gyro_pit) + " \n" +\
						"UAV IMU Roll: " + str(self.uav_gyro_rol) + " \n")
				if id_byte in ('m',):
					self.uav_mag_x = struct.unpack("h",data[1:3])[0]
					self.uav_mag_y = struct.unpack("h",data[3:5])[0]
					self.uav_mag_z = struct.unpack("h",data[5:7])[0]
					root.ids.u_ifp.ids.u_info5.text = str( \
						"UAV mag X: " + str(self.uav_mag_x) + " \n" +\
						"UAV mag Y: " + str(self.uav_mag_y) + " \n" +\
						"UAV mag Z: " + str(self.uav_mag_z) + " \n")

		except socket.error, msg:
			print "read sock errored:", str(msg[0])
			sock_in.close()
		finally:
			count_live_threads-=1
			#print "read server finished"
			

	#@TODO: restart the socket when settings are changed.
	def start_socket(self, val):
		self.server_address = (self.ids.u_uav_ip.text, int(self.ids.u_uav_port.text))
		self.server_in_addr = ("0.0.0.0",int(self.ids.u_uav_port.text)+1)
		try:
			self.sock_in.bind(self.server_in_addr)
		except socket.error, msg:
			print str(msg[0])
			print "cannot bind socket for listening, system exit."
			sys.exit()
		finally:
			print "sock_in bounded."
		
	p_throttle = ObjectProperty(None)
	p_altitude = ObjectProperty(None)
	def act(self):
		#print self.p_throttle.value
		sent = self.sock.sendto("T"+str(self.p_throttle.value), self.server_address)
		#print sent

	def res(self):
		#print self.p_altitude.value
		sent = self.sock.sendto("A"+str(self.p_altitude.value), self.server_address)

	def stop(self):
		sent = self.sock.sendto("S00", self.server_address)
	
class InfoPan(Widget):
	pass

class SettingPan(Widget):
	pass

class LabInput(Widget):
	pass
def f_uptime(total_seconds):
	MINUTE  = 60
	HOUR    = MINUTE * 60
	DAY     = HOUR * 24

	# Get the days, hours, etc:
	days    = int( total_seconds / DAY )
	hours   = int( ( total_seconds % DAY ) / HOUR )
	minutes = int( ( total_seconds % HOUR ) / MINUTE )
	seconds = int( total_seconds % MINUTE )

	# Build up the pretty string (like this: "N days, N hours, N minutes, N seconds")
	string = ""
	if days > 0:
	 string += str(days) + " " + (days == 1 and "day" or "days" ) + ", "
	if len(string) > 0 or hours > 0:
	 string += str(hours) + " " + (hours == 1 and "hour" or "hours" ) + ", "
	if len(string) > 0 or minutes > 0:
	 string += str(minutes) + " " + (minutes == 1 and "minute" or "minutes" ) + ", "
	string += str(seconds) + " " + (seconds == 1 and "second" or "seconds" )

	return string;


root = Builder.load_string("""
#:kivy 1.9.1
#:import sys sys
#:import MapSource mapview.MapSource
#:import join os.path.join
#:import dirname os.path.dirname
#:import StringProperty kivy.properties.StringProperty
#The Controller Panel
<Controller>:
	p_throttle:u_throttle
	p_altitude:u_altitude
    canvas.before:
        Color:
            rgba: 1, 1, 0, .2
        Rectangle:
            size: self.size
            pos: self.pos
	GridLayout:
		size: root.size
		rows: 2
		cols: 1
		BoxLayout:
			MapView:
				id:u_mapvew
				lat: -27.6
		        lon: 153.0
		        zoom: 13
		        map_source: MapSource(sys.argv[1], attribution="") if len(sys.argv) > 1 else "osm"
		        MapMarker:
		        	id:u_mapmrk
			        lat: -27.6
		        	lon: 153.0
		        MapMarker:
		        	id:u_mapmrk_home
			        lat: -27.6
		        	lon: 153.0
		        	#works in windows
		        	#source:  "C:\\Users\\qq_2\\Documents\\untangle\\untangle_rpi\\kivytest01\\mapview\\icons\\home.png"
		        	#source:  StringProperty(join(dirname(__file__), "icons", "home.png"))
		GridLayout:
			rows:1
			cols:4
			BoxLayout:
				size_hint:(0.2,1)
				orientation:'vertical'
				Slider:
					orientation:'vertical'
					max:100
					min:60
					value:60
					id:u_throttle
					on_value:root.act()
					step:1
				Label:
					text:"T"+str(u_throttle.value)
					size_hint:(1,.2)
			BoxLayout:
				size_hint:(0.2,1)
				orientation:'vertical'
				Slider:
					orientation:'vertical'
					max:2000
					min:0
					value:0
					id:u_altitude
					on_value:root.res()
					step:1
				Label:
					text:"A" +str(u_altitude.value)
					size_hint:(1,.2)
			GridLayout:
				padding:[30,30]
				rows:2
				cols:1
				BoxLayout:
					size_hint:(1, 0.3)
					orientation:'horizontal'
					CheckBox:
						id:u_sticky_swtch
						size_hint:(0.05,2)
						active:True
					Label:
						size:root.size
						size_hint:(0.95,2)
						halign:'left'
						text:"StickyGPS"
				Button:
					font_size:30
					background_color:[1,0,0,1]
					text:"STOP"
					on_release: root.stop()
			GridLayout:
				padding:[30,30]
				rows:3
				cols:3
				Label:
				Button:
					font_size:30
					background_color:[.5,1,0,1]
					text:"N"
				Label:
				Button:
					font_size:30
					background_color:[.5,1,0,1]
					text:"W"
				BoxLayout:
					padding:[5,5]
					orientation:"horizontal"
					Button:
						font_size:20
						background_color:[1,1,0,1]
						text:"Home"
				Button:
					font_size:30
					background_color:[.5,1,0,1]
					text:"E"
				Label:
				Button:
					font_size:30
					background_color:[.5,1,0,1]
					text:"S"
				Label:

#DebugPanel
<InfoPan>:
	canvas.before:
        Color:
            rgba: 0, 0, 0, 0
        Rectangle:
            size: self.size
            pos: self.pos
	GridLayout:
		pos: root.pos
		size: root.size
		rows: 5
		cols: 1
		Label:
			id:u_info1
			text:"test"
			text_size: self.size
    		halign: 'left'
    		valign: 'top'
    	Label:
			id:u_info2
			text:"test"
			text_size: self.size
    		halign: 'left'
    		valign: 'top'
    	Label:
			id:u_info3
			text:"test"
			text_size: self.size
    		halign: 'left'
    		valign: 'top'
    	Label:
			id:u_info4
			text:"test"
			text_size: self.size
    		halign: 'left'
    		valign: 'top'
    	Label:
			id:u_info5
			text:"test"
			text_size: self.size
    		halign: 'left'
    		valign: 'top'

#Settings n Connections
<SettingPan>:
	canvas.before:
		Color:
			rgba: 1,1,1,0.3
		Rectangle:
			size: self.size
			pos:self.pos
	GridLayout:
		pos:root.pos
		size:root.size
		rows:10
		cols:2
		Label:
			text:"UAV:IP"
		TextInput:
			id:u_uav_ip
			text:"192.168.1.104"
		Label:
			text:"UAV:PORT"
		TextInput:
			id:u_uav_port
			text:"40000"
		Label:
			text:""
		TextInput:
			text:""
		Label:
			text:""
		TextInput:
			text:""
		Label:
			text:""
		TextInput:
			text:""
		Label:
			text:""
		TextInput:
			text:""
		Label:
			text:""
		TextInput:
			text:""
		Label:
			text:""
		TextInput:
			text:""
		Label:
			text:""
		TextInput:
			text:""
		Label:
			text:""
		TextInput:
			text:""

TabbedPanel:
	u_cc:u_cc
	u_ifp:u_ifp
	u_set:u_set
	id:test
	do_default_tab:False
	TabbedPanelItem:
        text: 'Control'
		Controller:
			id:u_cc
	TabbedPanelItem:
        text: 'Info'
		InfoPan:
			id:u_ifp
	TabbedPanelItem:
        text: 'Settings'
		SettingPan:
			id:u_set

""")

#class MainApp(App):
#	def build(self):
#		return MainFrame()
configUAV={}

if __name__ == '__main__':
	loadFromConfig()
	runTouchApp(root)
	#MainApp().run()


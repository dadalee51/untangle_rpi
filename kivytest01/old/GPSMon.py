import atexit
import json
import sys
import socket
import mapview
import os
from kivy.app import App
from kivy.base import runTouchApp
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.uix.widget import Widget
from kivy.properties import NumericProperty, ObjectProperty
from kivy.uix.textinput import TextInput
from kivy.uix.tabbedpanel import TabbedPanel

if __name__ == '__main__' and __package__ is None:
    from os import sys, path
    sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

kwargs = {}
if len(sys.argv) > 1:
    kwargs["map_source"] = MapSource(url=sys.argv[1], attribution="")

#global methods

@atexit.register
def saveToConfig():
	print "Shutting down uav controller, saving configuration."
	configFN = 'uav_config.json'
	with open(configFN, 'w+') as f:
		global configUAV
		#save the ids to configUAV and write to file.
		configUAV['u_uav_ip'] = root.ids.u_set.ids.u_uav_ip.text
		configUAV['u_uav_port'] = root.ids.u_set.ids.u_uav_port.text
		#...more...
		json.dump(configUAV, f)
		f.close()

def loadFromConfig():
	global configUAV
	configFN = 'uav_config.json'
	if not os.path.exists(configFN):
		f = open(configFN, 'w+')
		f.close()
	try:
		f = open(configFN, 'r+')
		configUAV = json.load(f)
	except ValueError:
		print "no data in configUAV at this time, its okay.", sys.exc_info()[0]
	except IOError:
		print "something wrong trying to open/create config file, sys exit."
		sys.exit(1)
	else:
		print "Config file loaded with error: " , sys.exc_info()[0]
	#maps configUAV to ids
	if configUAV:
		if configUAV['u_uav_ip']:
			root.ids.u_set.ids.u_uav_ip.text = configUAV['u_uav_ip']
			root.ids.u_set.ids.u_uav_port.text = configUAV['u_uav_port']
	#do this regardless.
	root.ids.u_cc.ids.u_uav_ip = root.ids.u_set.ids.u_uav_ip 	
	root.ids.u_cc.ids.u_uav_port = root.ids.u_set.ids.u_uav_port

#class defs
#Controller will have a socket client to connect to UAV.
class Controller(Widget):
	sock = None
	server_address = None
	def __init__(self, **kwargs):
		super(Controller, self).__init__(**kwargs)
		Clock.schedule_once(self.startSocket, 0)

	def startSocket(self, val):
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		self.server_address = (self.ids.u_uav_ip.text, int(self.ids.u_uav_port.text))
		#print "ServerAddress:" , self.server_address


	p_throttle = ObjectProperty(None)
	p_altitude = ObjectProperty(None)
	def act(self):
		#print self.p_throttle.value
		sent = self.sock.sendto("T"+str(self.p_throttle.value), self.server_address)
		#print sent

	def res(self):
		#print self.p_altitude.value
		sent = self.sock.sendto("A"+str(self.p_altitude.value), self.server_address)

	def stop(self):
		sent = self.sock.sendto("S00", self.server_address)

class InfoPan(Widget):
	pass

class SettingPan(Widget):
	pass

class LabInput(Widget):
	pass

root = Builder.load_string("""
#:kivy 1.9.1
#:import sys sys
#:import MapSource mapview.MapSource

#The Controller Panel
<Controller>:
	p_throttle:u_throttle
	p_altitude:u_altitude
    canvas.before:
        Color:
            rgba: 1, 1, 0, .2
        Rectangle:
            size: self.size
            pos: self.pos
	GridLayout:
		size: root.size
		rows: 2
		cols: 1
		BoxLayout:
			MapView:
				lat: -27.6
		        lon: 153.0
		        zoom: 13
		        map_source: MapSource(sys.argv[1], attribution="") if len(sys.argv) > 1 else "osm"
		        MapMarker:
			        lat: -27.6
		        	lon: 153.0
		GridLayout:
			rows:1
			cols:4
			BoxLayout:
				size_hint:(0.2,1)
				orientation:'vertical'
				Slider:
					orientation:'vertical'
					max:100
					min:60
					value:60
					id:u_throttle
					on_value:root.act()
					step:1
				Label:
					text:"T"+str(u_throttle.value)
					size_hint:(1,.2)
			BoxLayout:
				size_hint:(0.2,1)
				orientation:'vertical'
				Slider:
					orientation:'vertical'
					max:100
					min:0
					value:0
					id:u_altitude
					on_value:root.res()
					step:1
				Label:
					text:"A" +str(u_altitude.value)
					size_hint:(1,.2)
			GridLayout:
				padding:[30,60]
				rows:1
				cols:1
				Button:
					font_size:30
					background_color:[1,0,0,1]
					text:"STOP"
					on_release: root.stop()
			GridLayout:
				padding:[30,30]
				rows:3
				cols:3
				Label:
				Button:
					font_size:30
					background_color:[.5,1,0,1]
					text:"N"
				Label:
				Button:
					font_size:30
					background_color:[.5,1,0,1]
					text:"W"
				BoxLayout:
					padding:[5,5]
					orientation:"horizontal"
					Button:
						font_size:20
						background_color:[1,1,0,1]
						text:"Home"
				Button:
					font_size:30
					background_color:[.5,1,0,1]
					text:"E"
				Label:
				Button:
					font_size:30
					background_color:[.5,1,0,1]
					text:"S"
				Label:

#DebugPanel
<InfoPan>:
	canvas.before:
        Color:
            rgba: 0, 0, 0, 0
        Rectangle:
            size: self.size
            pos: self.pos
	GridLayout:
		pos: root.pos
		size: root.size
		rows: 1
		cols: 1
		Label:
			id:u_info
			text:"test"
			text_size: self.size
    		halign: 'left'
    		valign: 'top'
#Settings n Connections
<SettingPan>:
	canvas.before:
		Color:
			rgba: 1,1,1,0.3
		Rectangle:
			size: self.size
			pos:self.pos
	GridLayout:
		pos:root.pos
		size:root.size
		rows:10
		cols:2
		Label:
			text:"UAV:IP"
		TextInput:
			id:u_uav_ip
			text:"192.168.1.104"
		Label:
			text:"UAV:PORT"
		TextInput:
			id:u_uav_port
			text:"40000"
		Label:
			text:""
		TextInput:
			text:""
		Label:
			text:""
		TextInput:
			text:""
		Label:
			text:""
		TextInput:
			text:""
		Label:
			text:""
		TextInput:
			text:""
		Label:
			text:""
		TextInput:
			text:""
		Label:
			text:""
		TextInput:
			text:""
		Label:
			text:""
		TextInput:
			text:""
		Label:
			text:""
		TextInput:
			text:""

TabbedPanel:
	u_cc:u_cc
	u_ifp:u_ifp
	u_set:u_set
	id:test
	do_default_tab:False
	TabbedPanelItem:
        text: 'Control'
		Controller:
			id:u_cc
	TabbedPanelItem:
        text: 'Info'
		InfoPan:
			id:u_ifp
	TabbedPanelItem:
        text: 'Settings'
		SettingPan:
			id:u_set

""")

#class MainApp(App):
#	def build(self):
#		return MainFrame()
configUAV={}

if __name__ == '__main__':
	loadFromConfig()
	runTouchApp(root)
	#MainApp().run()


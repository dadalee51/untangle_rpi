"""
untangle_server for control interface. Datagram and I2C
"""
import atexit
import socket
import struct
import sys
import smbus
import subprocess
import time 
from array import array
from threading import Thread, Lock,Condition
from Queue import Queue

#helpers
def rindex(l, v):
    return len(l) - l[::-1].index(v) - 1

#def mypack(f):
#	return 

#before everything, we want ALL other service to be ready before the 
#server is launched - 3 minutes = 3 * 60 * 60 = 10800
#time.sleep(10800) - use this in the script that starts it...

try:
	hole = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
except socket.error, msg:
	print "could not create socket, Err:" + str(msg[0])
	sys.exit()
try:
	#s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	#s.connect(("gmail.com",80)) #obtain ip from external connection.
	#vname = s.getsockname()[0]
	#print "using", vname, "as socket ip"
	#s.close()
	#hole.bind((vname,40000))
	hole.bind(("0.0.0.0",40000))
except socket.error, msg:
	print "Bind failed, Err:" + str(msg[0])
	sys.exit()
	
print "starting up server"
bus = smbus.SMBus(1)
DEVICE_ADDR = 0x04 #this is arduino
#BARO_ADDR = 0x68
#MPU_ADDR = 0x77
#MAG_ADDR = ??? #to be released from MPU

#Lock object for i2c critical area.
glock = Condition()
#the challenge trying to read from arduino is its got bad char
#after every read. We need to filter them.
def readArd(qa):
	while True:
		try:
			#print "qlock ac @readArd"
			glock.acquire()
			glock.wait()
			time.sleep(0.1) #wait for arduino to process data.
			data = bus.read_i2c_block_data(DEVICE_ADDR,0,16)
			barr = array('B', data)
			print barr #we will truncate any 0x0A or 0xFF ... maybe not 0A
			#if 0x0A in barr:
			#	nix = barr.index(0x0A)
			#	print "nix is", nix 
			#	barr = barr[:nix]
			#else
			if 0xFF in barr:
				nix = barr.index(0xFF)
				#print "nix is", nix 
				barr = barr[:nix]
			pdata = barr.tostring()
			print "readArd:", pdata
			#TODO: pass the data back to client.
		except IOError:
			print "IOError.. ignore..."
			time.sleep(0.1)
		finally:
			glock.release()
			#print "glock rl @readArd"
		time.sleep(0.0005)
		#ba = bytearray(struct.pack("f", 5.113))  
		#above will turn float in to series of 4 bytes.


#read socket, do some functional mapping and put Queue.
def readSock(qs):
	try:
		while True:
			data, addr = hole.recvfrom(16)
			if data:
				if data[0]=="T":
					amos =  bytes(int(float(data[1:])))
					qs.put("aT,"+amos)
				if data[0]=="A":
					amos =  bytes(float(data[1:]))
					qs.put("aA,"+amos)
				if data=="S00":
					qs.put("aS,00")
	finally:
		hole.close()

#houseKeeping job - if there are no message in q for 5 seconds then
#put in uR message.
def houseKeep(qh):
	while True:
		time.sleep(5)
		if qh.empty():
			qh.put("uR,11")
			#print 'just putted uR,11'

q = Queue()
t1 = Thread(target=readArd, args=(q,))
t2 = Thread(target=readSock, args=(q,))
t3 = Thread(target=houseKeep, args=(q,))
t1.start()
t2.start()
t3.start()

#main loop - dispatch the q content to arduino here.
while True:
	
	if not q.empty():
		bmos = q.get()
		print bmos
		cat = bmos.split(",")
		body = 0
		head = map(ord, cat[0])
		#print "head is:", head
		if head[1] == ord('T'):
			body = int(cat[1])
			head.append(body)
		if head[1] == ord('A'):
			#print "Before Pack:", float(cat[1])
			body = bytearray(struct.pack("f", float(cat[1])))
			#print body
			for bodypart in body:
				head.append(bodypart)
		if head[1] == ord('S'):
			body = int(cat[1])
			head.append(body)
		print "after append, head is:", head
		try:
			#print "glock ac @main"
			glock.acquire()
			
			#print 'writing ', head, 'to device..'
			bus.write_block_data(DEVICE_ADDR,0,head)
		except IOError:
			try:
				#subprocess.call("i2cdetect", "-y", "1")
				print "i2c interface write problem..."
			except TypeError:
				print "Arduino..."
		finally:
			glock.notify()
			glock.release()
			#print "glock rl @main"
		
		time.sleep(0.025)
#exit hook.
@atexit.register
def progExit():
	print "Exiting UAV Server..."



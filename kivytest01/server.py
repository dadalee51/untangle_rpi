#!/usr/bin/python
"""
untangle_server for control interface. Datagram and I2C
"""
import atexit
import Adafruit_BMP.BMP085 as BMP085
import serial
import socket
import struct
import string
import sys
import smbus
import subprocess
import time 
import math
import numpy
import roblib
import psutil, os, gc
from array import array
from threading import Thread, Lock,Condition
from Queue import Queue
from qk import QuickKalman

#set highest priority
p = psutil.Process(os.getpid())
p.nice(10)

#disable gc
gc.disable()

#init var area
sample_cntr = 200 #this equates to about 14 seconds after server starts.
lis_port = 40000
snd_port = lis_port + 1
qk_crash = QuickKalman(0.001, 0.001, 0.001)
qk_height = QuickKalman(0.001, 0.001, 0.001)
qk_mag_x = QuickKalman(0.001, 0.001, 0.001)
qk_mag_y = QuickKalman(0.001, 0.001, 0.001)
qk_mag_z = QuickKalman(0.001, 0.001, 0.001)
mag_round = 0
qk_gps_lat = QuickKalman(0.0001, 0.0001, 0.1)
qk_gps_lng = QuickKalman(0.0001, 0.0001, 0.1)
qk_gps_lat2 = QuickKalman(0.01, 0.01, 0.001) #second degree filter
qk_gps_lng2 = QuickKalman(0.01, 0.01, 0.001) #second degree filter
gps_round = 3

#helpers
def rindex(l, v):
    return len(l) - l[::-1].index(v) - 1

#connection setups
try:
	aCon = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	bCon = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) #the same address with port +1 to send back to client.
except socket.error, msg:
	print "could not create socket, Err:" + str(msg[0])
	sys.exit()
try:
	aCon.bind(("0.0.0.0",lis_port))
except socket.error, msg:
	print "Bind failed, Err:" + str(msg[0])
	sys.exit()

try:
	#GPS signal reading
	ser = serial.Serial('/dev/ttyS0', 9600)
except Exception, msg:
	print "Serial Exception during setup, Err:" + str(msg[0])
	sys.exit()
	
try:
	#setup barometer
	bmp180 = BMP085.BMP085()
except Exception, msg:
	print "Barometer setup error.", msg
	sys.exit()

print "starting up server"
bConIsBound = False #global flag marking bCon can be bound.
bus = smbus.SMBus(1)
ARDU_ADDR = 0x04 #this is arduino
BARO_ADDR = 0x68
MPU_ADDR = 0x77
#MAG_ADDR = 0x0C #to be released from MPU
MAG_ADDR = 0x1E #standalone module
#setup gyro, magnetomeger.
roblib.setup()


#Conditions for i2c critical area. I2C may have its own synchronization
# methods, however we are placing control here to make sure 
# it works without any gambling.
i2cCW = Condition() #i2cCW = dimension for arduino write.
i2cCR = Condition() #i2cCR = dimension for arduino read.

"""
read the imu for ypr data.

note that since the throttle value is a set-and-forget mechanism,
this makes any consecutive command easy to send adjustment values. 
we can later implement a command thread to do things like return to
home when uptime reached 5 minutes, or distance from home is greater 
than 2 km. 

the PID controller here will output [aP,(<255),(<255),(<255)] 

"""
def readGyro(qs, qa):
	#we will hardcode our gains until autotune is implemented.
	
	kP = 1.1
	kI = 1.07
	kD = 0.1
	e = [0.0] * 3 #error = diff by desired - gylp
	t = [0.0] * 3 #target
	o = [0.0] * 3 #old value, stored.
	d = [0.0] * 3 #difference of current and old value, stored.
	i = [0.0] * 3 #sum of current and old value, stored.
	r = [0.0] * 3 #resultant value.
	num_sample = 3
	global sample_cntr
	desired = [[0.0 for mi in range(3)] for mj in range(3)]
	while True:
		outY = bytearray("y",) #stands for ypr display
		outP = bytearray("aP", ) #stands for pid adjustment
		outI = bytearray("aP", ) #pid adjustment for arduino read int.
		try:
			i2cCR.acquire()
			gylp = roblib.loop()
			if gylp is not None :
				body = bytearray(struct.pack("f", gylp[0]))
				for b in body:
					outY.append(b)
				body = bytearray(struct.pack("f", gylp[1]))
				for b in body:
					outY.append(b)
				body = bytearray(struct.pack("f", gylp[2]))
				for b in body:
					outY.append(b)
				qs.put(outY)
				
				if sample_cntr >= num_sample:
					sample_cntr -= 1
				elif sample_cntr < -1:
					#here we update the pid.
					#gylp[0] is our current value.
					for x in range(0,3):
						e[x] = t[x] - gylp[x]
						d[x] = o[x] - e[x]
						i[x] = (i[x]*i[x]) + e[x] - o[x]
						r[x] = (kP*e[x] + kI*i[x] + kD*d[x])
						if r[x] > 20:
							r[x]=20
							i[x]=0
						elif r[x] < -20:
							r[x]=-20
							i[x]=0
						#then update old value
						o[x] = e[x]
						#print "g[",x,"]=", gylp[x], "t[",x,"]=",t[x], "e[",x,"]=",e[x], "i[",x,"]=",i[x], "d[",x,"]=",d[x], "r[",x,"]=",r[x]
						#print "d[",x,"]=",d[x]
						#print "e[",x,"]=",e[x]
						#print "r[",x,"]=",r[x]
					#then push r to the output queue.
					#body = bytearray(struct.pack("fff", r[0], r[1], r[2]))
					body = bytearray(struct.pack("hhh", int(r[0]), int(r[1]), int(r[2])))
					bodyi = bytearray(struct.pack("hhh", int(r[0]), int(r[1]), int(r[2])))
					#print int(r[0]), " ", int(r[1]), " ", int(r[2])
					#print int(m*r[1])
					#print "len of byte array is:",len(body) #12
					for b in body:
						outP.append(b)
					qs.put(outP)
					for bi in bodyi:
						#print int(bi)
						outI.append(bi)
					qa.put(outI)
					
				elif sample_cntr == -1:
					for x in range(0,3):
						t[x] = getAvg(desired[x])
						print "avg sample is:", t[x]
					sample_cntr-=1
				else: #sample_cntr starts from (num_sample-1) to 0
					#sample now
					print "sample_cntr is ", sample_cntr
					for x in range(0,3):
						desired[x][sample_cntr] = gylp[x]
					sample_cntr -= 1
				#time.sleep(0.1) #stable
				#time.sleep(0.1) #test
				
				#print "sample_cntr:",sample_cntr
					 
		except IOError:
			#print "IOError.."
			#time.sleep(1)
			pass
		finally:
			i2cCR.release()

#util func, getAverageValue
def getAvg(inList):
	print "list of average:",inList
	return sum(inList)/float(len(inList))

#readArd will only acquire the uptime and the ultrasonic height data -
# but fast because its intensive.
def readArd(qa):
	while True:
		try:
			i2cCR.acquire()
			i2cCR.wait()
			#time.sleep(0.01)
			data = bus.read_i2c_block_data(ARDU_ADDR,0,9)
			#time.sleep(0.025) #allow some time for ard to switch
			barr = array('B', data)
			#print "barr:",barr 
			qa.put(barr)
		except IOError, msg:
			#print "IOError.. ignore...", msg
			time.sleep(0.1)
		finally:
			i2cCR.release()

#tool method
def bytes_to_int(bytes):
	result = 0
	for b in bytes:
		result = (result << 8 ) | int(b)
	a = twosToInt(result, 16)
	return a

def twosToInt(val, len):
		# Convert twos compliment to integer
		if(val & (1 << len - 1)):
			val = val - (1<<len)

		return val


#readMagnetCompass
def readMag(qs):
	magr = [None]*3
	while True:
		outm = bytearray("m",)
		try:
			i2cCR.acquire()
			data = bus.read_i2c_block_data(MAG_ADDR,0,10)
			barr = array('B', data)
			#print "barr:",barr
			barr = array('B', data)[3:9]
			#carr = list(barr[i] for i in [0,2,4]) #mpu9250
			carr = list(barr[i] for i in [0,1,2,3,4,5])
			m0 = bytes_to_int(carr[0:2])
			m1 = bytes_to_int(carr[2:4])
			m2 = bytes_to_int(carr[4:6])
			#print "mag:",carr 
			#print m0, m1, m2
			magr[0] = int(qk_mag_x.next(m0))
			magr[1] = int(qk_mag_y.next(m1))
			magr[2] = int(qk_mag_z.next(m2))
			#print "mag_qk:", magr
			body = bytearray(struct.pack("h", magr[0]))
			for b in body:
				outm.append(b)
			body = bytearray(struct.pack("h", magr[1]))
			for b in body:
				outm.append(b)
			body = bytearray(struct.pack("h", magr[2]))
			for b in body:
				outm.append(b)
			qs.put(outm)
		except IOError:
			print "IOError.. ignore..."
			time.sleep(0.1)
		finally:
			i2cCR.release()
			time.sleep(0.001)

def readGps(qs):
	last_pos = (0.0,0.0)
	print "Starting GPS thread"
	while True:
		try:
			outg = bytearray("g",)
			#GPS signal
			gpsdata=ser.readline()
			if gpsdata.startswith ('$GPRMC'):
				#then we parse this line
				#print(string.strip(gpsdata))
				gpsdata = string.strip(gpsdata)
				lat = gpsdata.split(",")[3]
				latd = gpsdata.split(",")[4] #if S then its negative
				if latd == "S" :
					latd ="-"
				else:
					latd = ""
				lng = gpsdata.split(",")[5]
				lngd = gpsdata.split(",")[6] #if W then negative
				if lngd == "W" :
					lngd = "-"
				else:
					lngd = ""
				latDD = lat[0:2]
				latMMMMM = lat[2:10]
				lngDD = lng[0:3]
				lngMMMMM = lng[3:11]
				#process qk
				lat_qk = qk_gps_lat.next(float(latMMMMM))
				lng_qk = qk_gps_lng.next(float(lngMMMMM))
				
				#output rounding
				rlat_qk = round(lat_qk, gps_round)
				rlng_qk = round(lng_qk, gps_round)
				
				#second tier damper
				lat_qk2 = qk_gps_lat2.next(rlat_qk)
				lng_qk2 = qk_gps_lng2.next(rlng_qk)
				
				#round again
				rlat_qk2 = round(lat_qk2, gps_round)
				rlng_qk2 = round(lng_qk2, gps_round)
				
				#compose degrees decimal
				dlat_qk2 = rlat_qk2/60 + float(latDD)
				dlng_qk2 = rlng_qk2/60 + float(lngDD)
				
				if latd=="-":
					dlat_qk2*=-1
				
				if lngd=="-":	
					dlng_qk2*=-1
				this_pos = (dlat_qk2, dlng_qk2)	

				last_pos = this_pos

				#output to client queue
				body = bytearray(struct.pack("f", dlat_qk2))
				for b in body:
					outg.append(b)
				body = bytearray(struct.pack("f", dlng_qk2))
				for b in body:
					outg.append(b)
					
				qs.put(outg)
				#print outg
		except ValueError:
			#GPS not wake up yet, wait for 5 seconds and retry.
			print "GPS value error, wait 5."
			time.sleep(5)
		except IndexError:
			#gps busy, give it some time.
			print "GPS parse error, wait 5."
			time.sleep(5)

#read socket, do some functional mapping and put Queue.
def readSock(qs):
	global bConIsBound
	try:
		while True:
			data, addr = aCon.recvfrom(16)
			if data:
				if data[0]=="T":
					amos =  bytes(int(float(data[1:])))
					qs.put("aT,"+amos)
				if data[0]=="A":
					amos =  bytes(float(data[1:]))
					qs.put("aA,"+amos)
				if data=="S00":
					qs.put("aS,00")
			if (not bConIsBound) and addr[0]:
				print "recevied from:", addr[0], " binding and start sendSock thread."
				#register to global returning address.
				try:
					#clear the queue, so we are sending out the latest messages.
					q2clnt.queue.clear()
					bConIsBound = True
					t4 = Thread(target=sendSock, args=(q2clnt,addr[0]))
					t4.start()
				except socket.error, msg:
					print "Bind failed for returning bCon, Err:" + str(msg[0])
				finally:
					print "finish bConBound"
	finally:
		print "end readSock"
		
#houseKeeping job - if there are no message in q for 5 seconds then
#put in uR message.
def houseKeep(qh):
	while True:
		time.sleep(0.2)
		#time.sleep(20)
		if qh.empty():
			qh.put("uR,11")

#task for sending information back to client
def sendSock(qs, ip):
	try:
		while True:
			msg = qs.get()
			sent = bCon.sendto(msg, (ip,snd_port))
	except:
		#print "something bad???"
		time.sleep(0.5)
	finally:
		pass

#barometer loop
def readBmp(qs):
	while True:	
		outb = bytearray("b",)
		bmpalt = bmp180.read_altitude()
		bmptmp = bmp180.read_temperature()
		body = bytearray(struct.pack("f", bmpalt))
		for b in body:
			outb.append(b)
		body = bytearray(struct.pack("f", bmptmp))
		for b in body:
			outb.append(b)
		qs.put(outb)
		#print "outb:", outb
		#print("bmp:" + str(bmpalt) + ',' + str(bmptmp))
		time.sleep(0.01) #stable
		#time.sleep(0.5)
		#time.sleep(10)


#deploy and dispatch queue and threads.
q = Queue()
q2clnt = Queue()
t1 = Thread(target=readArd, args=(q2clnt,))
t2 = Thread(target=readSock, args=(q,))
t3 = Thread(target=houseKeep, args=(q,))
t5 = Thread(target=readGps, args=(q2clnt,))
t6 = Thread(target=readBmp, args=(q2clnt,))
t7 = Thread(target=readGyro, args=(q2clnt,q))
t8 = Thread(target=readMag, args=(q2clnt,))

t1.start()
t2.start()
t3.start()
t5.start()
t6.start()
t7.start()
t8.start()



print "\nAll threads have been dispatched!\n"

#main loop - dispatch the q content to arduino here.
while True:
	
	if not q.empty():
		try:
			bmos = q.get()
			if bmos[0:2]=="aP":
				i2cCW.acquire()
				bmos = list(bmos)
				#print bmos
				bus.write_block_data(ARDU_ADDR,0,bmos)
				#time.sleep(0.001)
			else:
				#print bmos #example: aT,63 or uR,11 then processed into 
				# byte array that has a pattern of [ aT(<255) ]
				#print q.qsize()
				cat = bmos.split(",")
				body = 0
				head = map(ord, cat[0])
				#print "head is:", head
				if head[1] == ord('T'):
					body = int(cat[1])
					head.append(body)
				elif head[1] == ord('A'):
					body = bytearray(struct.pack("f", float(cat[1])))
					for bodypart in body:
						head.append(bodypart)
				elif head[1] == ord('S'):
					body = int(cat[1])
					head.append(body)
				#otherwise, just send the packet as is.
				
				i2cCW.acquire()
				bus.write_block_data(ARDU_ADDR,0,head)
				#time.sleep(0.001)
				#if we have sent an request packet, wake the client read lock.
				if head[0] == ord('u'):
					i2cCR.acquire()
					i2cCR.notify()
					i2cCR.release()
		except IOError:
			print "IOError!"
		finally:
			i2cCW.notify()
			i2cCW.release()
		#time.sleep(0.001)
#exit hook.
@atexit.register
def progExit():
	print "Exiting UAV Server..."
	aCon.close()
	bCon.close()
	ser.close()

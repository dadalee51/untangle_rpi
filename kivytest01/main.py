import atexit
import json
import sys
import socket
import struct
import time
import mapview
import os
import io
import gc
import pdb
from os.path import join, dirname, normpath
from Queue import Queue
from threading import Thread, Lock,Condition
from itertools import chain
from kivy.app import App
from kivy.base import runTouchApp
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.uix.widget import Widget
from kivy.properties import NumericProperty, ObjectProperty,StringProperty
from kivy.uix.textinput import TextInput
from kivy.uix.tabbedpanel import TabbedPanel
from kivy.uix.image import Image
from kivy.core.image import Image as CoreImage
from kivy.logger import Logger
from graph import MeshLinePlot


seg_size = 10000 #this is consistent in server and client.

if __name__ == '__main__' and __package__ is None:
	from os import sys, path
	sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

Logger.info("main started")

kwargs = {}
if len(sys.argv) > 1:
	kwargs["map_source"] = MapSource(url=sys.argv[1], attribution="")

#global methods

@atexit.register
def saveToConfig():
	print "Shutting down uav controller, saving configuration."
	configFN = 'uav_config.json'
	with open(configFN, 'w+') as f:
		global configUAV
		#save the ids to configUAV and write to file.
		configUAV['u_uav_ip'] = root.ids.u_set.ids.u_uav_ip.text
		configUAV['u_uav_port'] = root.ids.u_set.ids.u_uav_port.text
		configUAV['u_vid_port'] = root.ids.u_set.ids.u_vid_port.text
		#...more...
		json.dump(configUAV, f)
		f.close()

def loadFromConfig():
	global configUAV
	configFN = 'uav_config.json'
	if not os.path.exists(configFN):
		f = open(configFN, 'w+')
		f.close()
	try:
		f = open(configFN, 'r+')
		configUAV = json.load(f)
	except ValueError:
		Logger.exception("no data in configUAV at this time, its okay.", sys.exc_info()[0])
	except IOError:
		Logger.exception("something wrong trying to open/create config file, sys exit.")

		sys.exit(1)
	else:
		print "Config file loaded with error: " , sys.exc_info()[0]
	#maps configUAV to ids
	if configUAV:
		if configUAV['u_uav_ip']:
			root.ids.u_set.ids.u_uav_ip.text = configUAV['u_uav_ip']
		if configUAV['u_uav_port']:
			root.ids.u_set.ids.u_uav_port.text = configUAV['u_uav_port']
		if configUAV['u_vid_port']:
			root.ids.u_set.ids.u_vid_port.text = configUAV['u_vid_port']
	#do this regardless.
	root.ids.u_cc.ids.u_uav_ip = root.ids.u_set.ids.u_uav_ip 	
	root.ids.u_cc.ids.u_uav_port = root.ids.u_set.ids.u_uav_port
	root.ids.u_cc.ids.u_vid_port = root.ids.u_set.ids.u_vid_port

#locks for sync accessing the shared var for threads.

class PlotData:
	def __init__(self):
		self.values1 = []
		self.values2 = []
		self.values3 = []
		self.plot1 = MeshLinePlot(color=[1, 0, 0, 1])
		self.plot2 = MeshLinePlot(color=[0, 1, 0, 1])
		self.plot3 = MeshLinePlot(color=[0, 0, 1, 1])

	def append(self,x,y,z):
		self.values1.append(x)
		self.values2.append(y)
		self.values3.append(z)

	#roll the values to correct size for display, tracking should be a negative number.
	def roll(self, tracking):
		val_range = 300
		self.values1 = self.values1[tracking:]
		self.values2 = self.values2[tracking:]
		self.values3 = self.values3[tracking:]
		self.plot1.points =  [(i, j) for i, j in enumerate(self.values1)]
		self.plot2.points =  [(i, j) for i, j in enumerate(self.values2)]
		self.plot3.points =  [(i, j) for i, j in enumerate(self.values3)]

#Controller will have a socket client to connect to UAV.
class Controller(Widget):
	Logger.info("starting Controller")
	sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	sock_in = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	server_address = None
	server_in_addr = None
	server_vid_in_addr = None
	uav_gps_lat_kal = None
	uav_gps_lon_kal = None

	pid_data = PlotData()
	imu_data = PlotData()
	mag_data = PlotData()
	
	q = Queue()

	p_throttle = ObjectProperty(None)
	p_altitude = ObjectProperty(None)

	def __init__(self, **kwargs):
		super(Controller, self).__init__(**kwargs)
		Clock.schedule_once(self.start_socket, 0)
		Clock.schedule_once(self.post_init, 0)
		Clock.schedule_once(self.start_read_server_thread, 1) #read server every 0.01 s
		Clock.schedule_once(self.start_update_plot_thread, 1)
		Clock.schedule_once(self.start_print_status_thread, 1)

	def post_init(self, val):
		#print root.ids.u_set.ids.u_uav_ip.text
		#print root.ids.u_set.ids.u_vid_port.text
		#print "rtsp://" + root.ids.u_set.ids.u_uav_ip.text  , ":" , root.ids.u_set.ids.u_vid_port.text , "/h264" )
		root.ids.u_cc.ids.u_img1.source = "rtsp://" + root.ids.u_set.ids.u_uav_ip.text  + ":" + root.ids.u_set.ids.u_vid_port.text + "/h264" 
		print root.ids.u_cc.ids.u_img1.source
		#print "current source is:",root.ids.u_cc.ids.u_mapmrk.source
		root.ids.u_cc.ids.u_mapmrk_home.source=join(dirname(__file__), "mapview", "icons", "home.png")
		#we also need to place the Line instructions here to draw the lines... so it dont get gced.
		root.ids.u_ifp.ids.u_info3_graph.add_plot(self.pid_data.plot1)
		root.ids.u_ifp.ids.u_info3_graph.add_plot(self.pid_data.plot2)
		root.ids.u_ifp.ids.u_info3_graph.add_plot(self.pid_data.plot3)
		root.ids.u_ifp.ids.u_info4_graph.add_plot(self.imu_data.plot1)
		root.ids.u_ifp.ids.u_info4_graph.add_plot(self.imu_data.plot2)
		root.ids.u_ifp.ids.u_info4_graph.add_plot(self.imu_data.plot3)
		root.ids.u_ifp.ids.u_info5_graph.add_plot(self.mag_data.plot1)
		root.ids.u_ifp.ids.u_info5_graph.add_plot(self.mag_data.plot2)
		root.ids.u_ifp.ids.u_info5_graph.add_plot(self.mag_data.plot3)

	def start_read_server_thread(self, val):
		#global pool
		t1 = Thread(target=self.read_server, args=(self.q,))
		#pool.append(t1)
		t1.daemon = True
		t1.start()
		
	def start_update_plot_thread(self,val):
		#global pool
		t1 = Thread(target=self.update_plot, args=[])
		#pool.append(t1)
		t1.daemon = True
		t1.start()
		
	def start_print_status_thread(self, q):
		#global pool
		t1 = Thread(target=self.print_status, args=(self.q,))
		#pool.append(t1)
		t1.daemon = True
		t1.start()

	#queue are for plotting graph in real time.
	def read_server(self,q):
		while True :
			try:
				#print "in readSock, trying recvfrom..."
				data, addr = self.sock_in.recvfrom(16)
				#print str(data[0])
				if data:
					id_byte = struct.unpack("c",data[0:1])[0]
					if id_byte in ('h', 'r'):
						uav_height = struct.unpack("H",data[1:3])[0]
						uav_uptime = struct.unpack("L",data[3:7])[0]
						uav_crash = struct.unpack("H",data[7:9])[0]
						q.put(['h',uav_height,uav_crash,uav_uptime])
					elif id_byte in ('g',):
						self.uav_gps_lat_kal = struct.unpack("f",data[1:5])[0]
						self.uav_gps_lon_kal = struct.unpack("f",data[5:9])[0]
						root.ids.u_cc.ids.u_mapmrk.lat=self.uav_gps_lat_kal
						root.ids.u_cc.ids.u_mapmrk.lon=self.uav_gps_lon_kal
						if root.ids.u_cc.ids.u_sticky_swtch.active :
							root.ids.u_cc.ids.u_mapvew.center_on(self.uav_gps_lat_kal, self.uav_gps_lon_kal)
						q.put(['l',self.uav_gps_lat_kal,self.uav_gps_lon_kal])
					elif id_byte in ('b',):
						self.uav_bmp_alt = struct.unpack("f",data[1:5])[0]
						self.uav_bmp_tmp = struct.unpack("f",data[5:9])[0]
						q.put(['a',self.uav_bmp_alt,self.uav_bmp_tmp])
					elif id_byte in ('y',):
						self.uav_gyro_yaw = struct.unpack("f",data[1:5])[0]
						self.uav_gyro_pit = struct.unpack("f",data[5:9])[0]
						self.uav_gyro_rol = struct.unpack("f",data[9:13])[0]
						q.put(['y',self.uav_gyro_yaw,self.uav_gyro_pit,self.uav_gyro_rol])
					elif id_byte in ('m',):
						self.uav_mag_x = struct.unpack("h",data[1:3])[0]
						self.uav_mag_y = struct.unpack("h",data[3:5])[0]
						self.uav_mag_z = struct.unpack("h",data[5:7])[0]
						q.put(['x',self.uav_mag_x,self.uav_mag_y,self.uav_mag_z])
					elif id_byte in ('a'):
						self.uav_pid_pit = struct.unpack("h",data[2:4])[0]
						self.uav_pid_rol = struct.unpack("h",data[4:6])[0]
						self.uav_pid_yaw = struct.unpack("h",data[6:8])[0]
						q.put(['p',self.uav_pid_pit,self.uav_pid_rol,self.uav_pid_yaw])

			except socket.error, msg:
				Logger.exception("read sock errored:", str(msg[0]))
			finally:
				time.sleep(0.0001)

	def update_plot(self):
		while True:
			try:
				self.imu_data.roll(-100)
				self.mag_data.roll(-100)
				self.pid_data.roll(-100)
			finally:
				time.sleep(0.0001)
	
	def print_status(self, q):
		while True:
			try:
				item = q.get()
				if item[0]=='h':
					root.ids.u_ifp.ids.u_info1.text = str( \
								"UAV Height: " + str(item[1]) + " cm\n" +\
								"UAV Crash: " + str(item[2]) + " cm\n" +\
								"UAV Uptime: " + self.f_uptime(item[3]/1000) + " sec\n")
					#root.ids.u_ifp.ids.u_info1_graph.values1.append(item[1])
					#root.ids.u_ifp.ids.u_info1_graph.values2.append(item[2])
				elif item[0]=='l':
					root.ids.u_ifp.ids.u_info2.text = str( \
						"UAV Latitude: " + str(item[1]) + "\n" +\
						"UAV Longitude: " + str(item[2]) + "\n")
					#root.ids.u_ifp.ids.u_info2_graph.values1.append(0)
					#root.ids.u_ifp.ids.u_info2_graph.values2.append(0)
				elif item[0]=='a':
					root.ids.u_ifp.ids.u_info3.text = str( \
						"UAV Barometer Altitude: " + str(item[1]) + " \n" +\
						"UAV Barometer Temperature: " + str(item[2]) + " \n")
				elif item[0]=='y':
					root.ids.u_ifp.ids.u_info4.text = str( \
						"UAV IMU Yaw: " + str(item[1]) + " \n" +\
						"UAV IMU Pitch: " + str(item[2]) + " \n" +\
						"UAV IMU Roll: " + str(item[3]) + " \n")
					self.imu_data.append(item[1],item[2],item[3])
				elif item[0]=='x':
					root.ids.u_ifp.ids.u_info5.text = str( \
						"UAV mag X: " + str(item[1]) + " \n" +\
						"UAV mag Y: " + str(item[2]) + " \n" +\
						"UAV mag Z: " + str(item[3]) + " \n")
					self.mag_data.append(item[1],item[2],item[3])
				elif item[0]=='p':
					self.pid_data.append(item[1],item[2],item[3])
				else:
					pass
			finally:
				time.sleep(0.0001)

	def f_uptime(self, total_seconds):
		MINUTE  = 60
		HOUR    = MINUTE * 60
		DAY     = HOUR * 24

		# Get the days, hours, etc:
		days    = int( total_seconds / DAY )
		hours   = int( ( total_seconds % DAY ) / HOUR )
		minutes = int( ( total_seconds % HOUR ) / MINUTE )
		seconds = int( total_seconds % MINUTE )

		# Build up the pretty string (like this: "N days, N hours, N minutes, N seconds")
		string = ""
		if days > 0:
		 string += str(days) + " " + (days == 1 and "day" or "days" ) + ", "
		if len(string) > 0 or hours > 0:
		 string += str(hours) + " " + (hours == 1 and "hour" or "hours" ) + ", "
		if len(string) > 0 or minutes > 0:
		 string += str(minutes) + " " + (minutes == 1 and "minute" or "minutes" ) + ", "
		string += str(seconds) + " " + (seconds == 1 and "second" or "seconds" )

		return string;

	#@TODO: restart the socket when settings are changed.
	def start_socket(self, val):
		self.server_address = (self.ids.u_uav_ip.text, int(self.ids.u_uav_port.text))
		self.server_in_addr = ("0.0.0.0",int(self.ids.u_uav_port.text)+1)
		try:
			self.sock_in.bind(self.server_in_addr)
		except socket.error, msg:
			Logger.exception(str(msg[0]))
			Logger.exception("cannot bind socket for listening, system exit.")
			sys.exit()
		finally:
			print "sock_in bounded."
		
	
	def act(self):
		#print self.p_throttle.value
		sent = self.sock.sendto("T"+str(self.p_throttle.value), self.server_address)
		#print sent

	def res(self):
		#print self.p_altitude.value
		sent = self.sock.sendto("A"+str(self.p_altitude.value), self.server_address)

	def stop(self):
		sent = self.sock.sendto("S00", self.server_address)
	
class InfoPan(Widget):
	pass

class SettingPan(Widget):
	pass

class LabInput(Widget):
	pass



root = Builder.load_string("""
#:kivy 1.9.0
#:import sys sys
#:import MapSource mapview.MapSource
#:import join os.path.join
#:import dirname os.path.dirname
#:import StringProperty kivy.properties.StringProperty
#:import Animation kivy.animation.Animation
#:import chain itertools.chain
#:import gauss random.gauss
#:import Clock kivy.clock.Clock
#:import MeshLinePlot graph.MeshLinePlot
#:import VideoPlayer kivy.uix.videoplayer.VideoPlayer
#The Controller Panel
<Controller>:
	p_throttle:u_throttle
	p_altitude:u_altitude
	canvas.before:
		Color:
			rgba: 1, 1, 0, .2
		Rectangle:
			size: self.size
			pos: self.pos
	GridLayout:
		size: root.size
		rows: 2
		cols: 1
		BoxLayout:
			orientation:'horizontal'
			MapView:
				id:u_mapvew
				lat: -27.6
				lon: 153.0
				zoom: 13
				map_source: MapSource(sys.argv[1], attribution="") if len(sys.argv) > 1 else "osm"
				MapMarker:
					id:u_mapmrk
					lat: -27.6
					lon: 153.0
				MapMarker:
					id:u_mapmrk_home
					lat: -27.6
					lon: 153.0
					#works in windows
					#source:  "C:\\Users\\qq_2\\Documents\\untangle\\untangle_rpi\\kivytest01\\mapview\\icons\\home.png"
					#source:  StringProperty(join(dirname(__file__), "icons", "home.png"))
			VideoPlayer:
				id:u_img1
				#source:"rtsp://192.168.1.115:40002/h264"
				#source:""
				state: 'stop'
		GridLayout:
			rows:1
			cols:4
			BoxLayout:
				size_hint:(0.2,1)
				orientation:'vertical'
				Slider:
					orientation:'vertical'
					max:95
					min:58
					value:58
					id:u_throttle
					on_value:root.act()
					step:1
				Label:
					text:"T"+str(u_throttle.value)
					size_hint:(1,.2)
			BoxLayout:
				size_hint:(0.2,1)
				orientation:'vertical'
				Slider:
					orientation:'vertical'
					max:2000
					min:0
					value:0
					id:u_altitude
					on_value:root.res()
					step:1
				Label:
					text:"A" +str(u_altitude.value)
					size_hint:(1,.2)
			GridLayout:
				padding:[30,30]
				rows:2
				cols:1
				BoxLayout:
					size_hint:(1, 0.3)
					orientation:'horizontal'
					CheckBox:
						id:u_sticky_swtch
						size_hint:(0.05,2)
						active:True
					Label:
						size:root.size
						size_hint:(0.95,2)
						halign:'left'
						text:"StickyGPS"
				Button:
					font_size:30
					background_color:[1,0,0,1]
					text:"STOP"
					on_release: root.stop()
			GridLayout:
				padding:[30,30]
				rows:3
				cols:3
				Label:
				Button:
					font_size:30
					background_color:[.5,1,0,1]
					text:"N"
				Label:
				Button:
					font_size:30
					background_color:[.5,1,0,1]
					text:"W"
				BoxLayout:
					padding:[5,5]
					orientation:"horizontal"
					Button:
						font_size:20
						background_color:[1,1,0,1]
						text:"Home"
				Button:
					font_size:30
					background_color:[.5,1,0,1]
					text:"E"
				Label:
				Button:
					font_size:30
					background_color:[.5,1,0,1]
					text:"S"
				Label:

#DebugPanel
<InfoPan>:
	#canvas.before:
	#	Color:
	#		rgba: 0, 0, 0, 0
	#	Rectangle:
	#		size: self.size
	#		pos: self.pos
	GridLayout:
		pos: root.pos
		size: root.size
		rows: 5
		cols: 2
		Label:
			id:u_info1
			text:"height,crash,uptime"
			text_size: self.size
			halign: 'left'
			valign: 'top'
		Label:
			id:u_text1
		Label:
			id:u_info2
			text:"lat,long"
			text_size: self.size
			halign: 'left'
			valign: 'top'
		Label:
			id:u_info2_graph
		Label:
			id:u_info3
			text:"bmp altitude, temperature"
			text_size: self.size
			halign: 'left'
			valign: 'top'
		Graph:
			id:u_info3_graph
			xlabel:"yX(red),pX(green),rX(blue)"
			ylabel:"diff"
			padding:2
			xmin:-0
			xmax:100
			ymin:-128
			ymax:128
		Label:
			id:u_info4
			text:"Yaw,Pitch,Roll"
			text_size: self.size
			halign: 'left'
			valign: 'top'
		Graph:
			id:u_info4_graph
			xlabel:"yaw(red),pitch(green),roll(blue)"
			ylabel:"degree"
			padding:2
			xmin:-0
			xmax:100
			ymin:-180
			ymax:180
		Label:
			id:u_info5
			text:"MagX,Y,Z"
			text_size: self.size
			halign: 'left'
			valign: 'top'
		Graph:
			id:u_info5_graph
			xlabel:"X(red),Y(green),Z(blue)"
			ylabel:"compass"
			padding:2
			xmin:-0
			xmax:100
			ymin:-512
			ymax:512

#Settings n Connections
<SettingPan>:
	canvas.before:
		Color:
			rgba: 1,1,1,0.3
		Rectangle:
			size: self.size
			pos:self.pos
	GridLayout:
		pos:root.pos
		size:root.size
		rows:10
		cols:2
		Label:
			text:"UAV:IP"
		TextInput:
			id:u_uav_ip
			text:"192.168.1.104"
		Label:
			text:"UAV:PORT"
		TextInput:
			id:u_uav_port
			text:"40000"
		Label:
			text:"VID:PORT"
		TextInput:
			id:u_vid_port
			text:"40002"
		Label:
			text:""
		TextInput:
			text:""
		Label:
			text:""
		TextInput:
			text:""
		Label:
			text:""
		TextInput:
			text:""
		Label:
			text:""
		TextInput:
			text:""
		Label:
			text:""
		TextInput:
			text:""
		Label:
			text:""
		TextInput:
			text:""
		Label:
			text:""
		TextInput:
			text:""

TabbedPanel:
	u_cc:u_cc
	u_ifp:u_ifp
	u_set:u_set
	id:test
	do_default_tab:False
	TabbedPanelItem:
		text: 'Control'
		Controller:
			id:u_cc
	TabbedPanelItem:
		text: 'Info'
		InfoPan:
			id:u_ifp
	TabbedPanelItem:
		text: 'Settings'
		SettingPan:
			id:u_set

""")

#class MainApp(App):
#	def build(self):
#		return MainFrame()
configUAV={}

if __name__ == '__main__':
	loadFromConfig()
	Logger.info("config passed")
	runTouchApp(root)
	#MainApp().run()


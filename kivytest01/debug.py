from kivy.app import App
from kivy.base import runTouchApp
from kivy.lang import Builder
from kivy.uix.widget import Widget

class InfoPan(Widget):
	pass

root = Builder.load_string("""
#:kivy 1.9.1
#:import sys sys
#:import StringProperty kivy.properties.StringProperty
#:import MeshLinePlot graph.MeshLinePlot
InfoPan:
	canvas.before:
		Color:
			rgba: 0, 0, 0, 0
		Rectangle:
			size: self.size
			pos: self.pos
	GridLayout:
		pos: root.pos
		size: root.size
		rows: 3
		cols: 2
		Label:
			id:u_info3
			text:"Description"
			text_size: self.size
			halign: 'left'
			valign: 'top'
		Graph:
			id:u_info3_graph
			value:[(0,1),(2,6)]
			xlabel:"A(red),B(green),C(blue)"
			ylabel:"diff"
			padding:2
			xmin:-0
			xmax:100
			ymin:-255
			ymax:255
		Label:
			id:u_info4
			text:"Description2"
			text_size: self.size
			halign: 'left'
			valign: 'top'
		Graph:
			id:u_info4_graph
			xlabel:"X,Y,Z"
			ylabel:"degree"
			padding:2
			xmin:-0
			xmax:100
			ymin:-180
			ymax:180
		Label:
			id:u_info5
			text:"Description3"
			text_size: self.size
			halign: 'left'
			valign: 'top'
		Graph:
			id:u_info5_graph
			xlabel:"X(red),Y(green),Z(blue)"
			ylabel:"Info"
			padding:2
			xmin:-0
			xmax:100
			ymin:-512
			ymax:512

""")
if __name__ == '__main__':
	runTouchApp(root)


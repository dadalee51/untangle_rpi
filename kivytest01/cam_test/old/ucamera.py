#!/usr/bin/python
import socket
import time
import picamera

with picamera.PiCamera() as camera:
	camera.resolution = (640, 480)
	#camera.resolution = (1920,1080)
	#camera.resolution = (1296,730)
	camera.vflip = True
	camera.framerate = 24
	#server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	server_socket = socket.socket()
	server_socket.bind(('0.0.0.0', 8000))
	server_socket.listen(0)
	# Accept a single connection and make a file-like object out of it
	connection = server_socket.accept()[0].makefile('wb')
	try:
		print "start record to connection..."
		#camera.start_recording(connection, format='h264')
		camera.start_recording(connection, format='mjpeg')
		camera.led = False
		#camera.wait_recording(60)
		#camera.stop_recording()
		while True:
			time.sleep(10)
			pass
	finally:
		camera.stop_recording()
		connection.close()
		server_socket.close()

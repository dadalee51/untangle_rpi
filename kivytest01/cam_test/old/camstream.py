#!/usr/bin/python
#test sending images over udp sockets.
import io
import time
import picamera
import socket
import struct

#create a sending socket with udp
#usock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
dest_addr = ("192.168.1.101", 40002)
usock = socket.socket()
usock.connect(dest_addr)
connection = usock.makefile('wb')
# Create an in-memory stream
stm = io.BytesIO()
cam = picamera.PiCamera()
#camera.start_preview()
# Camera warm-up time

time.sleep(2)

cntr = 30
try:
	for fn in cam.capture_continuous(stm, 'jpeg'):
		#obtain the size of capture, send little endian, long.
		connection.write(struct.pack('<L', stm.tell()))
		connection.flush()
		print "size of stream:", stm.tell()
		#read all from stream and send through socket. 
		
		
		stm.seek(0)
		print "send all from stm"
		connection.write(stm.read())
		#usock.recv(16)
		print "sendto finished."
		#cut off stream
		#time.sleep(2)
		stm.seek(0)
		stm.truncate()
		cntr-=1
		if cntr <0 : break
except Exception, msg:
	print "Error", msg

finally:
	print "program finished."
	usock.close()
	
	

